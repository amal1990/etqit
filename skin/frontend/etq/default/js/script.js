(function($) {

  $.fn.menumaker = function(options) {
      
      var cssmenu = $(this), settings = $.extend({
        title: "Menu",
        format: "dropdown",
        sticky: false
      }, options);

      return this.each(function() {
        cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
        $(this).find("#menu-button").on('click', function(){
          $(this).toggleClass('menu-opened');
		  
	
          var mainmenu = $(this).next('ul');
          if (mainmenu.hasClass('open')) { 
            mainmenu.hide().removeClass('open');
				$('.submenu-button').each(function(index, element) {
					var $this=$(this);
                    if($this.hasClass('submenu-opened')){
						$this.trigger('click');
					}
                });
				
          }
          else {
            mainmenu.show().addClass('open');
            if (settings.format === "dropdown") {
              mainmenu.find('ul').show();
            }
          }
        });

        cssmenu.find('li ul').parent().addClass('has-sub');

        multiTg = function() {
          cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
          cssmenu.find('.submenu-button').on('click', function() {
            $(this).toggleClass('submenu-opened');
            if ($(this).siblings('ul').hasClass('open')) {
              $(this).siblings('ul').removeClass('open').hide();
            }
            else {
              $(this).siblings('ul').addClass('open').show();
            }
          });
        };

        if (settings.format === 'multitoggle') multiTg();
        else cssmenu.addClass('dropdown');

        if (settings.sticky === true) cssmenu.css('position', 'fixed');

        resizeFix = function() {
			//alert($( window ).width())
          if ($( window ).width() > 1024) {
            cssmenu.find('ul').show();
			//alert("test")
          }

          if ($(window).width() <= 1024) {
            cssmenu.find('ul').hide().removeClass('open');
			$('.submenu-button').each(function(index, element) {
					var $this=$(this);
                    if($this.hasClass('submenu-opened')){
						$this.trigger('click');
					}
                });
			$('#menu-button').removeClass('menu-opened');	
          }
        };
        resizeFix();
        return $(window).on('orientationchange', resizeFix);

      });
  };
})(jQuery);

(function($){
$(document).ready(function(){

$("#cssmenu").menumaker({
   title: "Menu",
   format: "multitoggle"
});

});
})(jQuery);
//$(window).load(function(e) {
//		$(document).on('mouseout','.magnify',function(){
//			var $this=$(this);
//			//alert('asdas');
//			$('.magnify-lens').addClass('opacityno');
//		});
//		$(document).on('mouseenter','.magnify',function(){
//			var $this=$(this);
//			//alert('lop');
//			$('.magnify-lens').removeClass('opacityno');
//		});
//});

