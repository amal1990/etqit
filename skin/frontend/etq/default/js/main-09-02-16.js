jQuery(window).load(function() {
  new WOW().init();	

	//Function to animate slider captions 
	function doAnimations( elems ) {
		//Cache the animationend event in a variable
		var animEndEv = 'webkitAnimationEnd animationend';
		
		elems.each(function () {
			var $this = jQuery(this),
				$animationType = $this.data('animation');
			$this.addClass($animationType).one(animEndEv, function () {
				$this.removeClass($animationType);
			});
		});
	}
	
	//Variables on page load 
	var $myCarousel = jQuery('#carousel-example-generic, #carousel-example-testimonial'),
		$firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
		
	//Initialize carousel 
	$myCarousel.carousel();
	
	//Animate captions in first slide on page load 
	doAnimations($firstAnimatingElems);
	
	//Pause carousel  
	$myCarousel.carousel('pause');
	
	
	//Other slides to be animated on carousel slide event 
	$myCarousel.on('slide.bs.carousel', function (e) {
		var $animatingElems = jQuery(e.relatedTarget).find("[data-animation ^= 'animated']");
		doAnimations($animatingElems);
	}); 
	$myCarousel.carousel({interval: 2000});
	
});



jQuery('.similar-product').owlCarousel({
		loop:true,
		margin:20,
		nav:true,
		responsiveClass:true,
	
		responsive:{
			0:{
				items:1,
				nav:true
			},
			
			480:{
				items:2,
				nav:true
			},
			
			600:{
				items:2,
			},
			1000:{
				items:4
			}
		}
	});


//for flexslider product slider
jQuery(window).load(function() {


  
//Owl carousel logo_slider initiation
jQuery('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,	
    navText: [
      "<i class='fa fa-arrow-circle-o-left'></i>",
      "<i class='fa fa-arrow-circle-o-right'></i>"
      ],
    responsive:{
        0:{
            items:1
        },
        767:{
            items:2
        },
        1000:{
            items:4
        }
    }
});

	
	
	
	
	  // Image Fit
    jQuery('.imageFit').imgLiquid({
        fill: true,
        horizontalAlign: 'center',
        verticalAlign: 'center'
    });
	
	
  jQuery('.logo-slider').owlCarousel({
		loop:true,
		nav:true,
		navText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		responsive:{
			0:{
				items:2
			},
			768:{
				items:4
			},
			1199:{
				items:8
			}
		}
	});
});

jQuery(document).ready(function($) {
//$('.pro_con').bind('mouseenter', function(){
//
//	$(this).find('.pro-overlay').show();
//}).bind('mouseleave', function(){
//
//	$(this).find('.pro-overlay').hide();
//});
//$('.product-image').bind('mouseenter', function(){
//
//	$(this).find('.hover-overlay').show();
//}).bind('mouseleave', function(){
//
//	$(this).find('.hover-overlay').hide();
//});
//
//
//
$( "#facacc" ).accordion({collapsible: true});

$(".close_compare").click(function(){
        $("#compare_block").css("display", "none");
         $(".close_compare").css("display", "none");
        });

$(document).on('mouseenter','.pro_con',function(){

$(this).find('.pro-overlay').show().addClass('hover-overlay');

});


$(document).on('mouseleave','.pro_con',function(){

$(this).find('.pro-overlay').hide();

});

$('.block-compare').scrollToFixed();

$('.similar-product').owlCarousel({
  loop:true,
  margin:20,
  nav:true,
  responsiveClass:true,
 
  responsive:{
   0:{
    items:1,
    nav:true
   },
   
   480:{
    items:2,
    nav:true
   },
   
   600:{
    items:2,
   },
   1000:{
    items:4
   }
  }
 });
$("#reviewpopup, #tabform").click(function(){
    jQuery.fancybox({
	    'type': 'inline',
	    'href': '#rpop',
	    autoSize : true,
	});
    
    });

// $('.nav li > div > ul')
//  .find('li:gt(5)') //you want :gt(4) since index starts at 0 and H3 is not in LI
//  .hide()
//  .end()
//  .each(function(){
//      if($(this).children('li').length > 5){ //iterates over each UL and if they have 5+ LIs then adds Show More...
//          $(this).append(
//              $('<li><i class="fa fa-plus-circle"></i>View More</li>')
//              .addClass('showMore')
//              .click(function(){
//                  if($(this).siblings(':hidden').length > 0){
//                      $(this).html('<i class="fa fa-minus-circle"></i>View Less').siblings(':hidden').show();
//                  }else{
//                      $(this).html('<i class="fa fa-plus-circle"></i>View More').show().siblings('li:gt(5)').hide();
//                  }
//                  $('.showMore').not(this).show().siblings('li:gt(4)').hide();
//              })
//          );
//     }
//  });
    $('#info').readmore({
      moreLink: '<a class="view-more" href="#"><i class="fa fa-plus-circle"></i> View More</li></a>',
	  lessLink: '<a class="view-more" href="#"><i class="fa fa-minus-circle"></i> View Less</a>',
      collapsedHeight: 180
    });

//$('.big_img img').magnify({
//		speed: 200,
//	      });
//$(document).on('click','.img_port img',function(){
//		var $this=$(this);
//		var this_src=$this.parent().attr('data-rel');
//		var org_img=$this.parent().attr('org-img');
//		$('.big_img img').attr('src',this_src);
//		$('.img_port').removeClass('active');
//		$this.parent().addClass('active');
//		
//		$('.big_img img').magnify({
//		speed: 200,
//		src: org_img
//	      });
//	});
if ($( window ).width() > 768) {
$("#zoom_03").elevateZoom({gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true});
}
else{
$(document).on('click','.img_port img',function(){
		var $this=$(this);
		var this_src=$this.parent().attr('data-rel');
		var org_img=$this.parent().attr('org-img');
		$('.big_img img').attr('src',this_src);
		$('.img_port').removeClass('active');
		$this.parent().addClass('active');
		
	//	$('.big_img img').magnify({
	//	speed: 200,
	//	src: org_img
	//      });
	});

  //$("#gallery_01 a").click(function() {
  //          var myVar = setInterval(function(){ // wait for fading
  //
  //              var height = $("#zoom_03").css("height");
  //              var width = $("#zoom_03").css("width");
  //              $(".zoomContainer").css("height", height);
  //              $(".zoomContainer").css("width", width);
  //              $(".zoomContainer .zoomWindow").css("height", height);
  //              $(".zoomContainer .zoomWindow").css("width", width);
  //
  //              clearInterval(myVar);
  //          }, 100);
  //      });
}

	$('.pro-slide').owlCarousel({
		//loop:true,
		margin:4,
		nav:true,
		responsiveClass:true,
			navText:["<i class='fa fa-chevron-left'></i>",
      "<i class='fa fa-chevron-right'></i>"],
		responsive:{
			0:{
				items:3,
				nav:true
			},
			
				360:{
				items:3,
			},
			
				480:{
				items:5,
			},
			600:{
				items:5,
			},
			
			
			768:{
				items:3,
			},
			1000:{
				items:5
			}
		}
	});
});







