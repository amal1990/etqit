<?php

class Uipl_Eventnews_Adminhtml_EventnewsController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("eventnews/eventnews")->_addBreadcrumb(Mage::helper("adminhtml")->__("Eventnews  Manager"),Mage::helper("adminhtml")->__("Eventnews Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("News"));
			    $this->_title($this->__("Manager News"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
			    $this->_title($this->__("Eventnews"));
				$this->_title($this->__("Eventnews"));
			    $this->_title($this->__("Edit Item"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("eventnews/eventnews")->load($id);
				if ($model->getId()) {
					Mage::register("eventnews_data", $model);
					$this->loadLayout();
					$this->_setActiveMenu("eventnews/eventnews");
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Eventnews Manager"), Mage::helper("adminhtml")->__("Eventnews Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Eventnews Description"), Mage::helper("adminhtml")->__("Eventnews Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
					$this->_addContent($this->getLayout()->createBlock("eventnews/adminhtml_eventnews_edit"))->_addLeft($this->getLayout()->createBlock("eventnews/adminhtml_eventnews_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("eventnews")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}

		public function newAction()
		{

		$this->_title($this->__("Eventnews"));
		$this->_title($this->__("Eventnews"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("eventnews/eventnews")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("eventnews_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("eventnews/eventnews");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Eventnews Manager"), Mage::helper("adminhtml")->__("Eventnews Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Eventnews Description"), Mage::helper("adminhtml")->__("Eventnews Description"));


		$this->_addContent($this->getLayout()->createBlock("eventnews/adminhtml_eventnews_edit"))->_addLeft($this->getLayout()->createBlock("eventnews/adminhtml_eventnews_edit_tabs"));

		$this->renderLayout();

		}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();
			
			//echo date('Y-m-d',strtotime(str_replace('/','-',$post_data['pubdate'])));
//print_r($post_data);exit;

				if ($post_data) {

					try {

						
				 //save image
		try{

if((bool)$post_data['image']['delete']==1) {

	        $post_data['image']='';

}
else {

	unset($post_data['image']);

	if (isset($_FILES))
		{
				if ($_FILES['image']['name'])
				{
						$file_size = $_FILES['image']['size'];
						if (($file_size > 2097152))
						{      
								$message = 'File too large. File must be less than 2 MB.'; 
								//echo '<script type="text/javascript">alert("'.$message.'");</script>';
								//Mage::getSingleton('adminhtml/session')->addError($message);
								//$this->_redirect('*/*/edit');
								//return;
								Mage::getSingleton('adminhtml/session')->addError($message);
								Mage::app()->getResponse()->setRedirect($_SERVER['HTTP_REFERER']);
								Mage::app()->getResponse()->sendResponse();
								exit;
						}
						if($this->getRequest()->getParam("id"))
						{
							$model = Mage::getModel("eventnews/eventnews")->load($this->getRequest()->getParam("id"));
							if($model->getData('image'))
							{
								$io = new Varien_Io_File();
								$io->rm(Mage::getBaseDir('media').DS.implode(DS,explode('/',$model->getData('image'))));	
							}
						}
						$path = Mage::getBaseDir('media') . DS . 'eventnews' . DS .'eventnews'.DS;
						$uploader = new Varien_File_Uploader('image');
						$uploader->setAllowedExtensions(array('jpg','png','gif'));
						$uploader->setAllowRenameFiles(false);
						$uploader->setFilesDispersion(false);
						
						$file = preg_replace('/\s+/', '_', $_FILES['image']['name']);
						
						$destFile = $path.$file;
						$filename = $uploader->getNewFileName($destFile);
						$uploader->save($path, $filename);
				
						$post_data['image']='eventnews/eventnews/'.$filename;
				}
		}
}

        } catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
        }
//save image
						$post_data['pubdate']=date('Y-m-d',strtotime($post_data['pubdate']));

						$model = Mage::getModel("eventnews/eventnews")
						->addData($post_data)
						->setId($this->getRequest()->getParam("id"))
						->save();

						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Eventnews was successfully saved"));
						Mage::getSingleton("adminhtml/session")->setEventnewsData(false);

						if ($this->getRequest()->getParam("back")) {
							$this->_redirect("*/*/edit", array("id" => $model->getId()));
							return;
						}
						$this->_redirect("*/*/");
						return;
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setEventnewsData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("eventnews/eventnews");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("eventnews/eventnews");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
			
		/**
		 * Export order grid to CSV format
		 */
		public function exportCsvAction()
		{
			$fileName   = 'eventnews.csv';
			$grid       = $this->getLayout()->createBlock('eventnews/adminhtml_eventnews_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
		} 
		/**
		 *  Export order grid to Excel XML format
		 */
		public function exportExcelAction()
		{
			$fileName   = 'eventnews.xml';
			$grid       = $this->getLayout()->createBlock('eventnews/adminhtml_eventnews_grid');
			$this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
		}
}
