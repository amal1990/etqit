<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Model_Bundle extends Mage_Core_Model_Abstract {

    protected $_product = null;
    protected $_selectionProducts = null;
    protected $_originalFinalAmount = null;
    protected $_originalAmount = null;
    protected $_needsConfiguration = null;
    protected $_selectionsLoaded = false;
    protected $_originalWeeeAmount = null;
    protected $_mainProduct = null;

    public function _construct() {
        parent::_construct();
        $this->_init('simplebundle/bundle');
    }

    public function getAmount() {
        return $this->getOriginalAmount(true) - $this->getDiscountAmountValue(false, true);
    }

    public function getDiscountAmountValue($calculationAmount = false, $forceFinalPrice = false) {
        $discountAmount = 0;
        if ($this->getDiscountType() == 'percent') {
            if(!$calculationAmount) {
                $calculationAmount = $this->getOriginalAmount($forceFinalPrice);
                if ($this->getExcludeBaseProductFromDiscount()) {
                    $calculationAmount -= $forceFinalPrice?$this->getBaseFinalAmount():$this->getBaseAmount();
                }
                if (!Mage::getStoreConfigFlag('simplebundle/general/apply_discount_on_weee')) {
                    $calculationAmount -= $this->_originalWeeeAmount;
                }
            }
            $discountAmount =  Mage::app()->getStore()->roundPrice($calculationAmount * $this->getDiscountAmount() / 100);
        } else {
            $discountAmount = $this->getDiscountAmount();
        }
        if($this->getSpecialPriceBehavior() == Webcooking_SimpleBundle_Helper_Data::SPECIAL_PRICE_BEHAVIOR_USE_LOWEST_PRICE && $this->getHasPromo()) {
            //if Use Lowest mode and has promo
            if(($this->getOriginalAmount() - $discountAmount) > $this->getOriginalAmount(true)) { 
                // if standard price - bundle discount is greater than the final price (= price including special price)
                // then we simply use the promo price
                $discountAmount = 0;
                $this->setUsedLowerPrice(true);
            } else {
                //else, we remove the promo part of the bundle discount
                $discountAmount = $discountAmount - ($this->getOriginalAmount() - $this->getOriginalAmount(true));
            }
        }
        return $discountAmount;
    }

    public function getDiscountPercentageValue($roundPrecision = 2, $originalAmount = false) {
        //if ($this->getDiscountType() == 'percent') {
        //    return $this->getDiscountAmount();
        //}  Discount Percent change if promo and promo behavior = use lowest price
        if (!$originalAmount) {
            $originalAmount = $this->getOriginalAmount();
        }
        $calculationAmount = $originalAmount;
        if ($this->getExcludeBaseProductFromDiscount()) {
            if(!$this->getBaseAmount()) {
                $this->getOriginalAmount();
            }
            $calculationAmount -= $this->getBaseAmount();
        }
        return round($this->getDiscountAmountValue($calculationAmount) * 100 / $calculationAmount, $roundPrecision);
    }
    

    public function getOriginalAmount($forceFinalPrice = false) {
        $optionPrices = array();
        if (Mage::registry('sb_options_prices')) {
            $optionPrices = Mage::registry('sb_options_prices');
        }
        if (is_null($this->_originalAmount)) {
            $products = $this->getAssociatedProducts();
            $products[] = array('product' => $this->getBaseProduct(), 'qty' => $this->getBaseQty());
            $amount = 0;
            $baseAmount = 0;
            $finalAmount = 0;
            $baseFinalAmount = 0;
            $hasPromo = false;
            $this->_originalWeeeAmount = 0;
            foreach ($products as $data) {
                $product = $data['product'];
                $quote = Mage::getSingleton('checkout/cart')->getQuote();
                
                $tierPrices = $product->getFormatedTierPrice();
                $qty = $data['qty'];
                
                if(!$hasPromo && Mage::helper('simplebundle')->isInPromo($product)) {
                    $hasPromo = true;
                }
               
                $productPrice = $product->getTypeId() == 'bundle' 
                        ? $product->getPriceModel()->getTotalPrices($product, 'min') 
                        : Mage::helper('tax')->getPrice($product, $product->getPrice(), Mage::helper('tax')->getPriceDisplayType() == Mage_Tax_Model_Config::DISPLAY_TYPE_BOTH ? true : null);
                $productFinalPrice = $product->getTypeId() == 'bundle' 
                        ? $product->getPriceModel()->getTotalPrices($product, 'min') 
                        : Mage::helper('tax')->getPrice($product, $product->getFinalPrice(), Mage::helper('tax')->getPriceDisplayType() == Mage_Tax_Model_Config::DISPLAY_TYPE_BOTH ? true : null);
              
                $weeeAmount = Mage::helper('weee')->getAmount($product);
                $productPrice += $weeeAmount;
                $productFinalPrice += $weeeAmount;
                $this->_originalWeeeAmount += $weeeAmount;
                //echo $product->getPriceModel()->getTotalPrices($product, 'min');
                $tierPriceQtyMatched = 0;
                $newProductPrice = $productPrice;
                $newProductFinalPrice = $productFinalPrice;
                foreach ($tierPrices as $tierPrice) {
                    if ($tierPrice['price_qty'] <= $qty && $tierPrice['price_qty'] > $tierPriceQtyMatched) {
                        $tierPriceQtyMatched = $tierPrice['price_qty'];
                        $newProductPrice = $product->getTypeId() == 'bundle' ? $productPrice - ($productPrice * $tierPrice['price'] / 100) : $tierPrice['price'];
                        $newProductFinalPrice = $product->getTypeId() == 'bundle' ? $productFinalPrice - ($productFinalPrice * $tierPrice['price'] / 100) : $tierPrice['price'];
                    }
                }
                $productPrice = $newProductPrice;
                $productFinalPrice = $newProductFinalPrice;
                if ($product->getId() == $this->getBaseProduct()->getId()) {
                    foreach ($optionPrices as $optionPrice) {
                        $optionPrice = explode(':', $optionPrice);
                        if ($optionPrice[1] == 'percent') {
                            $productPrice += $optionPrice[0] * $productPrice / 100;
                            $productFinalPrice += $optionPrice[0] * $productFinalPrice / 100;
                        } else {
                            $productPrice += $optionPrice[0];
                            $productFinalPrice += $optionPrice[0];
                        }
                    }
                }
                if ($product->getId() == $this->getProductId()) {
                    $baseAmount = $productPrice * $qty;
                    $baseFinalAmount = $productFinalPrice * $qty;
                }
                $amount += $productPrice * $qty;
                $finalAmount += $productFinalPrice * $qty;
            }
            $this->_originalAmount = Mage::app()->getStore()->roundPrice($amount);
            $this->_originalFinalAmount = Mage::app()->getStore()->roundPrice($finalAmount);
            $this->setBaseAmount($baseAmount);
            $this->setBaseFinalAmount($baseFinalAmount);
            $this->setHasPromo($hasPromo);
        }
        if($this->getDiscountAmount() && !$forceFinalPrice && $this->getSpecialPriceBehavior() == Webcooking_SimpleBundle_Helper_Data::SPECIAL_PRICE_BEHAVIOR_USE_LOWEST_PRICE && $this->getHasPromo()) {
            return $this->_originalAmount;
        }
        return $this->_originalFinalAmount;
    }

    public function getProduct() {
        return $this->getBaseProduct();
    }

    public function getBaseProduct() {
        if (is_null($this->_product)) {
            $this->_product = Mage::getModel('catalog/product')->load($this->getProductId());
        }
        return $this->_product;
    }

    public function getAssociatedProducts() {
        if (is_null($this->_selectionProducts)) {
            $this->_selectionProducts = array();
            $collection = Mage::getModel('simplebundle/bundle_item')->getCollection();
            $collection->addFieldToFilter('simple_bundle_id', $this->getId());
            foreach ($collection as $item) {
                $product = Mage::getModel('catalog/product')->load($item->getProductId());
                $this->_selectionProducts[] = array('product' => $product, 'qty' => $item->getSelectionQty());
            }
        }
        return $this->_selectionProducts;
    }

    protected function _productNeedsConfiguration($product) {
        return Mage::helper('simplebundle')->productNeedsConfiguration($product);
    }

    public function needsConfiguration() {
        if (is_null($this->_needsConfiguration)) {
            $this->_needsConfiguration = false;
            if ($this->_productNeedsConfiguration($this->getBaseProduct())) {
                $this->_needsConfiguration = true;
            }
            foreach ($this->getAssociatedProducts() as $product) {
                if ($this->_productNeedsConfiguration($product['product'])) {
                    $this->_needsConfiguration = true;
                }
            }
        }
        return $this->_needsConfiguration;
    }

    public function isSalable() {
        $isSalable = $this->getMainProduct()->isSaleable();
        if (!$isSalable) {
            return false;
        }
        $this->loadSelections(true);
        foreach ($this->getSelections() as $item) {
            if (!$item->getIsSalable()) {
                return false;
            }
        }
        return true;
    }
    
    public function hasSpecialPrice() {
        $isInPromo = Mage::helper('simplebundle')->isInPromo($this->getMainProduct());
        if ($isInPromo) {
            return true;
        }
        $this->loadSelections(true);
        foreach ($this->getSelections() as $item) {
            if (Mage::helper('simplebundle')->isInPromo($item)) {
                return true;
            }
        }
        return false;
    }
    
    public function getMainProduct() {
        if(is_null($this->_mainProduct)) {
            $this->_mainProduct = Mage::getModel('catalog/product')->load($this->getProductId());
        }
        return $this->_mainProduct;
    }

    public function loadSelections($withAttributes = true) {
        if ($this->_selectionsLoaded) {
            return;
        }

        $collection = Mage::getModel('simplebundle/bundle_item')->getCollection();
        $collection->addFieldToFilter('simple_bundle_id', $this->getId());
        if ($withAttributes) {
            foreach ($collection as $item) {
                $product = Mage::getModel('catalog/product')->load($item->getProductId(), array('name', 'sku', 'small_image', 'short_description'));
                $item->setName($product->getName());
                $item->setSku($product->getSku());
                $item->setSmallImage($product->getSmallImage());
                $item->setIsSalable($product->isSaleable());
            }
        }

        $this->setSelections($collection);

        $this->_selectionsLoaded = true;
    }

    protected function _beforeSave() {
        if(!$this->getData('created_at') || $this->getData('created_at') == '0000-00-00 00:00:00') {
            $this->setData('created_at', $this->_getResource()->formatDate(true));
        }
        $this->setData('updated_at', $this->_getResource()->formatDate(true));
    }

    protected function _afterSave() {
        if ($this->dataHasChangedFor('template_id') && $this->getTemplateId()) {
            $this->getTemplate()->updateBundle($this);
        }
    }

    public function getTemplate() {
        if ($this->getTemplateId()) {
            return Mage::getModel('simplebundle/bundle_template')->load($this->getTemplateId());
        }
        return false;
    }
    
    public function getSpecialPriceBehavior() {
        if(!$this->hasData('special_price_behavior') || !array_key_exists(intval($this->getData('special_price_behavior')), Mage::helper('simplebundle')->getSpecialPriceBehaviorOptions())) {
            $this->setData('special_price_behavior', Mage::helper('simplebundle')->getSpecialPriceBehavior());
        }
        return $this->getData('special_price_behavior');
    }

}
