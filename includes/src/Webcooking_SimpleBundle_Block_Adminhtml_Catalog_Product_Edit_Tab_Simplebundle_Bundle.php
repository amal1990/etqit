<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Block_Adminhtml_Catalog_Product_Edit_Tab_Simplebundle_Bundle extends Mage_Adminhtml_Block_Widget {

    protected $_bundles = null;
    public function __construct() {
        parent::__construct();
        $this->setTemplate('webcooking/simplebundle/catalog/product/edit/tab/simplebundle/bundle.phtml');
        $this->setCanReadPrice(true);
        $this->setCanEditPrice(true);
    }

    public function getFieldId() {
        return 'product_simplebundle';
    }
    
      public function getProduct()
    {
        return Mage::registry('product');
    }

     protected function _prepareLayout()
    {
        $this->setChild('add_selection_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'id'    => $this->getFieldId().'_{{index}}_add_button',
                    'label'     => Mage::helper('simplebundle')->__('Add Selection'),
                    'on_click'   => 'sbSelection.showSearch(event)',
                    'class' => 'add'
                )));

        $this->setChild('close_search_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'id'    => $this->getFieldId().'_{{index}}_close_button',
                    'label'     => Mage::helper('simplebundle')->__('Close'),
                    'on_click'   => 'sbSelection.closeSearch(event)',
                    'class' => 'back no-display'
                )));

        $this->setChild('option_delete_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label' => Mage::helper('simplebundle')->__('Delete Bundle'),
                    'class' => 'delete delete-product-option',
                    'on_click' => 'sbBundle.remove(event)'
                ))
        );

        $this->setChild('selection_template',
            $this->getLayout()->createBlock('simplebundle/adminhtml_catalog_product_edit_tab_simplebundle_bundle_selection')
        );

        return parent::_prepareLayout();
    }
    
    
    public function getAddButtonId() {
        $buttonId = $this->getParentBlock()
                        ->getChild('add_button')->getId();
        return $buttonId;
    }
    
    public function getFieldName() {
        return 'simplebundle';
    }
    
    public function getTemplateSelectHtml() {
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setData(array(
                'id' => $this->getFieldId().'_{{index}}_template_id',
                'class' => 'select select-product-bundle-template_id required-bundle-select'
            ))
            ->setName($this->getFieldName().'[{{index}}][template_id]')
            ->setOptions(Mage::helper('simplebundle')->getTemplateOptions());

        return $select->getHtml();
    }   
    
    public function getBoolSelectHtml($id='active',  $trueFirst=true)
    {
        $options = Mage::getSingleton('adminhtml/system_config_source_yesno')->toOptionArray();
        if(!$trueFirst) {
            $options = array_reverse($options);
        }
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setData(array(
                'id' => $this->getFieldId().'_{{index}}_'.$id,
                'class' => 'select select-product-bundle-'.$id.' required-bundle-select'
            ))
            ->setName($this->getFieldName().'[{{index}}]['.$id.']')
            ->setOptions($options);

        return $select->getHtml();
    }
    
    public function getDiscountTypeSelectHtml()
    {
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setData(array(
                'id' => $this->getFieldId().'_{{index}}_discount_type',
                'class' => 'select select-product-bundle-discount_type required-bundle-select'
            ))
            ->setName($this->getFieldName().'[{{index}}][discount_type]')
            ->setOptions(array(
                'percent' => $this->__('Percentage'),
                'fixed' => $this->__('Fixed')
            ));

        return $select->getHtml();
    }
    
    public function getSpecialPriceBehaviorSelectHtml() {
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setData(array(
                'id' => $this->getFieldId().'_{{index}}_special_price_behavior',
                'class' => 'select select-product-bundle-special_price_behavior required-bundle-select'
            ))
            ->setName($this->getFieldName().'[{{index}}][special_price_behavior]')
            ->setOptions(Mage::helper('simplebundle')->getSpecialPriceBehaviorOptions());

        return $select->getHtml();
    }
    
    public function getStoreSelectHtml()
    {
        $storeOptions = Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, false);
        $count = count($storeOptions);
        foreach($storeOptions as $storeOption) {
            if(count($storeOption['value'])>0)
                $count += count($storeOption['value']);
        }
        $select = $this->getLayout()->createBlock('adminhtml/html_select')
            ->setData(array(
                'id' => $this->getFieldId().'_{{index}}_stores_select',
                'class' => 'select select-product-bundle-stores-select required-bundle-select',
                'extra_params' => 'multiple="multiple" size="'.$count.'" style="display:none;"'
            ))
            ->setName($this->getFieldName().'[{{index}}][stores_select]')
            ->setOptions($storeOptions);

        return $select->getHtml();
    }
    
    public function getAddButtonHtml()
    {
        return $this->getChildHtml('add_button');
    }

    public function getCloseSearchButtonHtml()
    {
        return $this->getChildHtml('close_search_button');
    }

    public function getAddSelectionButtonHtml()
    {
        return $this->getChildHtml('add_selection_button');
    }
    
    public function getOptionDeleteButtonHtml() {
        return $this->getChildHtml('option_delete_button');
    }
    
    public function getSelectionHtml()
    {
        return $this->getChildHtml('selection_template');
    }
    
    
     public function getSimpleBundles()
    {
        if (!$this->_bundles) {
            $this->_bundles = Mage::getModel('simplebundle/bundle')->getCollection()
                    ->addFieldToFilter('product_id', $this->getProduct()->getId())
                    ->skipAllChecks()
                    ->appendSelections();
        }
        return $this->_bundles;
    }
    
}
