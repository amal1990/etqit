<?php
class Uipl_Specialoffer_Block_Adminhtml_Specialoffer_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("specialoffer_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("specialoffer")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("specialoffer")->__("Item Information"),
				"title" => Mage::helper("specialoffer")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("specialoffer/adminhtml_specialoffer_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
