<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Helper_Data extends Webcooking_All_Helper_Data {

    const SPECIAL_PRICE_BEHAVIOR_USE_LOWEST_PRICE = 1;
    const SPECIAL_PRICE_BEHAVIOR_DISABLE = 2;
    const SPECIAL_PRICE_BEHAVIOR_CUMULATE = 3;
    
    public function formatDescription($description) {
        if(!$description)
            return $this->__('Bundle discounts');
        return $this->__($description);
    }
    
    public function excludeBaseProductFromDiscount($store = null)
    {
        return Mage::getStoreConfigFlag('simplebundle/general/exclude_base_product_from_discount', $store);
    }
    
    public function getSpecialPriceBehavior($store = null)
    {
        return Mage::getStoreConfigFlag('simplebundle/general/special_price_behavior', $store);
    }
    
    public function productNeedsConfiguration($product) {
        if(!$product) return true;
        return $product->getTypeId() == 'configurable' || $product->getTypeId() == 'bundle' || count($product->getOptions())>0;
    }
    
    public function displayAllProductsInPopup($store = null) {
        return Mage::getStoreConfigFlag('simplebundle/popup/display_all_products', $store);
    }
    
    
    public function distinguishVariables($text, $productId, $extra=array()) {
        $text = preg_replace('%opConfig([^0-9])%', 'opConfig'.$productId.'$1', $text);
        $text = preg_replace('%spConfig([^0-9])%', 'spConfig'.$productId.'$1', $text);
        $text = preg_replace('%bundle([^0-9_/])%', 'bundle'.$productId.'$1', $text);
        $text = preg_replace('%optionFileUpload([^0-9])%', 'optionFileUpload'.$productId.'$1', $text);
        $text = preg_replace('%Product.Options([^0-9P])%', 'Product.Options'.$productId.'$1', $text);
        $text = preg_replace('%dateOption([^0-9])%', 'dateOption'.$productId.'$1', $text);
        $text = preg_replace('%optionsPrice([^0-9])%', 'optionsPrice'.$productId.'$1', $text);
        $text = preg_replace('%Product.Config([^0-9.])%', 'Product.Config'.$productId.'$1', $text);
        $text = preg_replace('%Product.Bundle([^0-9])%', 'Product.Bundle'.$productId.'$1', $text);
        $text = preg_replace('%product-options-wrapper([^0-9])%', 'product-options-wrapper'.$productId.'$1', $text);
        $text = preg_replace("%product_addtocart_form([^-])%", "product_addtocart_form-".$productId.'$1', $text);
        $text = preg_replace("%(body\s|')\.product-custom-option%", "$1.product-".$productId." .product-custom-option", $text);
        $text = preg_replace("%var\s%", "", $text);
        $text = preg_replace("%tierPriceInclTax, tierPriceExclTax;%", "var tierPriceInclTax, tierPriceExclTax;", $text);
        $text = preg_replace("%bundle = new Product%", "var bundle = new Product", $text);
        $text = preg_replace("%event = %", "var event = ", $text);
        $text = preg_replace("%/\*var\*/%", "var ", $text);
        //scp compatibility
        $text = preg_replace('%SCPcustomOptionsDiv([^0-9])%', 'SCPcustomOptionsDiv'.$productId.'$1', $text);
        $text = preg_replace('%scp-please-wait([^0-9])%', 'scp-please-wait'.$productId.'$1', $text);
        //cjm color selector plus compatibility
        $text = str_replace("$('image')", "$('image-".$productId."')", $text);
        $text = preg_replace("%ul-moreviews([^0-9])%", "ul-moreviews-".$productId.'$1', $text);
        $text = preg_replace("%doPreSelection([^0-9])%", "doPreSelection".$productId.'$1', $text);
        $text = preg_replace("%clicker([^0-9])%", "clicker".$productId.'$1', $text);
        $text = preg_replace("%setClickHandler([^0-9])%", "setClickHandler".$productId.'$1', $text);
        $text = preg_replace("%colorSelected([^0-9])%", "colorSelected".$productId.'$1', $text);
        //amasty conf extension compatibility
        //$text = preg_replace("%AmConfigurableData([^0-9])%", "AmConfigurableData".$productId.'$1', $text);
        //$text = preg_replace("%confData([^0-9])%", "confData".$productId.'$1', $text);
        //$text = preg_replace("%amconf([^0-9])%", "amconf".$productId.'$1', $text);
       
        
        
        
        
        //We don't want to modify main product price
        $text = preg_replace("%([^-])product-price-".$productId."%", "$1sb-product-price-".$productId."", $text);
        $text = preg_replace("%([^-])bundle-price-".$productId."%", "$1sb-bundle-price-".$productId."", $text);
        $text = preg_replace("%([^-])price-including-tax-".$productId."%", "$1sb-price-including-tax-".$productId."", $text);
        $text = preg_replace("%([^-])price-excluding-tax-".$productId."%", "$1sb-price-excluding-tax-".$productId."", $text);
        $text = preg_replace("%([^-])old-price-".$productId."%", "$1sb-old-price-".$productId."", $text);
              
        
        foreach($extra as $placeholder) {
            $text = preg_replace('%'.$placeholder.'([^0-9])%', $placeholder.$productId.'$1', $text);
        }
        
        return $text;
    }
    
    
    public function getBundleCollectionForProduct($product, $limit = null) {
        if(!$product || !is_object($product)) {
            return new Varien_Data_Collection();
    	}
        $collection = Mage::getModel('simplebundle/bundle')->getCollection()
               ->addFieldToFilter('active', 1)
               ->addFieldToFilter('product_id', $product->getId())
               ->addStoreFilter();
        if($limit && is_int($limit)) {
            $collection->setPageSize($limit);
        }
        $collection->appendSelections();
       return $collection;
    }
    
    public function getSpecialPriceBehaviorOptions($simpleArray = true) {
        $options = array(
            Webcooking_SimpleBundle_Helper_Data::SPECIAL_PRICE_BEHAVIOR_USE_LOWEST_PRICE => $this->__('Use lowest price'),
            Webcooking_SimpleBundle_Helper_Data::SPECIAL_PRICE_BEHAVIOR_DISABLE => $this->__('Disable Bundle'),
            Webcooking_SimpleBundle_Helper_Data::SPECIAL_PRICE_BEHAVIOR_CUMULATE => $this->__('Cumulate'),
        );
        if(!$simpleArray) {
            $options2 = array();
            foreach($options as $k=>$v) {
                $options2[] = array('value' => $k, 'label' => $v);
            }
            return $options2;
        }
        return $options;
    }
    
    public function getTemplateOptions($simpleArray = true) {
        $templateCollection = Mage::getModel('simplebundle/bundle_template')->getCollection();
        $options =  array('0'=> $this->__('None'));
        foreach($templateCollection as $template) {
            $options[$template->getId()] = str_replace("'", "\'", $this->escapeHtml($template->getName()));
        }
        if(!$simpleArray) {
            $options2 = array();
            foreach($options as $k=>$v) {
                $options2[] = array('value' => $k, 'label' => $v);
            }
            return $options2;
        }
        return $options;
    }
    
    public function isInPromo($product, $decimals = 0, $withSymbol = true, $allowEmptyEndDate = true) {
        if (!$product)
            return false;

        if ($product->getFinalPrice() >= $product->getPrice())
            return false;

        $percentage = (100 - round((100 * $product->getFinalPrice()) / $product->getPrice(), $decimals));

        if ($withSymbol)
            $percentage .= '%';

        return $percentage;
    }
    
    
    public function addBundleToCart($cart, $bundleId, $params) {
        $options = isset($params['options']) ? $params['options'] : array();
        $superAttributes = isset($params['super_attribute']) ? $params['super_attribute'] : array();
       
        
        
        $bundle = Mage::getModel('simplebundle/bundle')->load($bundleId);
        $baseProductId = $bundle->getProductId();
        $baseProductQty = $bundle->getBaseQty();
        $selection = array($baseProductId => $baseProductQty);
        foreach ($bundle->getAssociatedProducts() as $_selection) {
            $selection[$_selection['product']->getId()] = $_selection['qty'];
        }



        $productsToAdd = array();
        foreach ($selection as $pId => $qty) {
            $product = Mage::getModel('catalog/product')
                    ->setStoreId(Mage::app()->getStore()->getId())
                    ->load($pId);
            if (!$product) {
                return false;
            }

            $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
            );

            $productToAdd = array('product' => $product, 'qty' => $filter->filter($qty));
            if ($pId == $baseProductId && $options) {
                $productToAdd['options'] = $options;
            }
            if ($pId == $baseProductId && $superAttributes) {
                $productToAdd['super_attribute'] = $superAttributes;
            }

            if(isset($params['sbdata']) && isset($params['sbdata'][$pId])) {
                $productToAdd = array_merge($productToAdd, $params['sbdata'][$pId]);
            }

            $additionnalParameters = $this->_getAdditionalParams($product, $cart);

            //Mage::log($additionnalParameters);
            $productsToAdd[] = array_merge($productToAdd, $additionnalParameters);
        }



        foreach ($productsToAdd as $productToAdd) {
            $product = $productToAdd['product'];
            unset($productToAdd['product']);
           // Mage::log($product, null, 'sb.api.debug.log', true);
           // Mage::log($productToAdd, null, 'sb.api.debug.log', true);
            $cart->addProduct($product, $productToAdd);
        }


        Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
        
        $simpleBundleIds = Mage::getSingleton('checkout/session')->getSimpleBundleIds();
        if (!$simpleBundleIds) {
            $simpleBundleIds = array();
        }
        $simpleBundleIds[] = $bundleId;
        $simpleBundleIds = array_unique($simpleBundleIds);
        Mage::getSingleton('checkout/session')->setSimpleBundleIds($simpleBundleIds);



        $cart->save();
        return true;
    }
    
    protected function _getAdditionalParams($origProduct, $cart) {
        
        $bundleOptions = Mage::helper('wcooall/product')->isBundleProductWithoutConfiguration($origProduct);
        
        $additional = array();
        
        if($bundleOptions) {
            $additional = $bundleOptions;
        }
        
        return $additional;
    }
    
    public function displayBundleList($product, $max = null) {
        $block = Mage::app()->getLayout()->createBlock('simplebundle/catalog_product_view_bundle');
        $block->setTemplate('webcooking/simplebundle/catalog/product/list/bundle.phtml');
        $block->setProduct($product);
        $block->setLimit($max);
        return $block->toHtml();
    }
    
}