<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Block_Adminhtml_Catalog_Product_Edit_Tab_Simplebundle extends Mage_Adminhtml_Block_Widget implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    
     protected $_product = null;
      public function __construct()
    {
        parent::__construct();
        $this->setSkipGenerateContent(true);
        $this->setTemplate('webcooking/simplebundle/catalog/product/edit/tab/simplebundle.phtml');
    }

    public function getTabUrl()
    {
        return $this->getUrl('adminhtml/simplebundle_bundle_product_edit/form', array('_current' => true));
    }

    public function getTabClass()
    {
        return 'ajax';
    }

    protected function _prepareLayout()
    {
        $this->setChild('add_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label' => Mage::helper('simplebundle')->__('Add New Bundle'),
                    'class' => 'add',
                    'id'    => 'add_new_bundle',
                    'on_click' => 'sbBundle.add()'
                ))
        );

        $this->setChild('bundle_box',
            $this->getLayout()->createBlock('simplebundle/adminhtml_catalog_product_edit_tab_simplebundle_bundle',
                'adminhtml.catalog.product.edit.tab.simplebundle.bundle')
        );

        return parent::_prepareLayout();
    }


    public function getFieldSuffix()
    {
        return 'product';
    }

    public function getProduct()
    {
        return Mage::registry('product');
    }

    
    public function getAddButtonHtml()
    {
        return $this->getChildHtml('add_button');
    }

    public function getBundleBoxHtml()
    {
        return $this->getChildHtml('bundle_box');
    }
    
   
    public function getTabLabel()
    {
        return $this->__('Simple Bundles');
    }
   
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }
     
    public function canShowTab()
    {
        return true;
    }
     
    public function isHidden()
    {
        return false;
    }
    
}
