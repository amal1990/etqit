<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Model_Api extends Mage_Api_Model_Resource_Abstract {

    public function addtocart($quoteId, $bundleId, $additionalData='', $store = null) {
        $quote = $this->_getQuote($quoteId, $store);
         if (empty($store)) {
            $store = $quote->getStoreId();
        }
         Mage::log($additionalData, null, 'sb.api.debug.log', true);
       
        if($additionalData) {
            $additionalData = Zend_Json::decode($additionalData);
        } else {
            $additionalData = array();
        }
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($store);

        $cart = Mage::getModel('checkout/cart')->setQuote($quote)->init();
        //Mage::log('addtocart : ', null, 'sb.api.debug.log', true);
        //Mage::log($quoteId, null, 'sb.api.debug.log', true);
        //Mage::log($bundleId, null, 'sb.api.debug.log', true);
        Mage::log($additionalData, null, 'sb.api.debug.log', true);
        //Mage::log($store, null, 'sb.api.debug.log', true);
        $simpleBundle = Mage::getModel('simplebundle/bundle')->load($bundleId);
        if (!$simpleBundle->getId()) {
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            $this->_fault('not_exists');
        }
        
        if(!Mage::helper('simplebundle')->addBundleToCart($cart, $bundleId, $additionalData)) {
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            $this->_fault('data_invalid');
        }
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        return true;
    }
    
    
    protected function _getQuote($quoteId, $store = null)
    {
        /** @var $quote Mage_Sales_Model_Quote */
        $quote = Mage::getModel("sales/quote");

        if (!(is_string($store) || is_integer($store))) {
            $quote->loadByIdWithoutStore($quoteId);
        } else {
            $storeId = $this->_getStoreId($store);

            $quote->setStoreId($storeId)
                    ->load($quoteId);
        }
        if (is_null($quote->getId())) {
            $this->_fault('quote_not_exists');
        }

        return $quote;
    }
    
    protected function _getStoreId($store = null)
    {
        if (is_null($store)) {
            $store = ($this->_getSession()->hasData($this->_storeIdSessionField)
                        ? $this->_getSession()->getData($this->_storeIdSessionField) : 0);
        }

        try {
            $storeId = Mage::app()->getStore($store)->getId();

        } catch (Mage_Core_Model_Store_Exception $e) {
            $this->_fault('store_not_exists');
        }

        return $storeId;
    }
    
}
