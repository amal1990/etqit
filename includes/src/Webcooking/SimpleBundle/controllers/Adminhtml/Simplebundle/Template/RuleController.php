<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Adminhtml_Simplebundle_Template_RuleController extends Mage_Adminhtml_Controller_Action {

    protected function _construct() {
        $this->setUsedModuleName('Webcooking_SimpleBundle');
    }

    protected function _initAction() {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
                ->_setActiveMenu('catalog/simplebundle')
                ->_addBreadcrumb(Mage::helper('simplebundle')->__('Templates Associations Rules'), Mage::helper('simplebundle')->__('Templates Associations Rules'))
        ;
        return $this;
    }

    public function newConditionHtmlAction() {
        $this->conditionsHtmlAction('conditions');
    }

    public function newActionsHtmlAction() {
        $this->conditionsHtmlAction('actions');
    }
    
    protected function conditionsHtmlAction($prefix) {
        $id = $this->getRequest()->getParam('id');
        $typeArr = explode('|', str_replace('-', '/', $this->getRequest()->getParam('type')));
        $type = $typeArr[0];

        $model = Mage::getModel($type)
                ->setId($id)
                ->setType($type)
                ->setRule(Mage::getModel('simplebundle/bundle_template_rule'))
                ->setPrefix($prefix);
        if (!empty($typeArr[1])) {
            $model->setAttribute($typeArr[1]);
        }

        if ($model instanceof Mage_Rule_Model_Condition_Abstract || $model instanceof Webcooking_All_Model_Rule_Condition_Abstract) {
            $model->setJsFormObject($this->getRequest()->getParam('form'));
            $html = $model->asHtmlRecursive();
        } else {
            $html = '';
        }
        $this->getResponse()->setBody($html);
    }
    
    
     public function resultgridAction() {
        
        if(!Mage::registry('simplebundle_template')) {
            Mage::register('simplebundle_template', Mage::getModel('simplebundle/bundle_template')->load($this->getRequest()->getParam('template_id')));
        }
        return $this->getResponse()->setBody(
                        $this->getLayout()
                                ->createBlock('simplebundle/adminhtml_template_rule_grid', 'template.rule.result.grid')
                                ->toHtml()
        );
    }
    
    public function applyAction() {
        $templateId = $this->getRequest()->getParam('template_id');
        $template = Mage::getModel('simplebundle/bundle_template')->load($templateId);
        if(!$template->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('simplebundle')->__('This template does not exist'));
            $this->_redirect('*/adminhtml_simplebundle_template/edit', array('template_id'=>$templateId));
            return;
        }
        $template->applyRule();
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('simplebundle')->__('Rule has been applied'));
        $this->_redirect('adminhtml/simplebundle_template/edit', array('template_id'=>$templateId));
            
    }
    
}
