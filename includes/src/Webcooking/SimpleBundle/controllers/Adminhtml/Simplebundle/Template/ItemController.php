<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Adminhtml_Simplebundle_Template_ItemController extends Mage_Adminhtml_Controller_Action {

    protected function _construct() {
        $this->setUsedModuleName('Webcooking_SimpleBundle');
    }

    protected function _initAction() {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
                ->_setActiveMenu('catalog/simplebundle')
                ->_addBreadcrumb(Mage::helper('simplebundle')->__('Templates Items'), Mage::helper('simplebundle')->__('Templates Item'))
        ;
        return $this;
    }

    
    public function openpopupAction() {
        if (!Mage::registry('simplebundle_template')) {
            Mage::register('simplebundle_template', Mage::getModel('simplebundle/bundle_template')->load($this->getRequest()->getParam('templateId')));
        }
        return $this->getResponse()->setBody(
                        $this->getLayout()
                                ->createBlock('simplebundle/adminhtml_template_edit_tab_bundle_popup', 'template.popup.add.product')
                                ->setTemplate('webcooking/simplebundle/template/edit/tab/bundle/popup.phtml')
                                ->toHtml()
        );
    }
    
    
    
    public function gridAction() {
        if (!Mage::registry('simplebundle_template')) {
            Mage::register('simplebundle_template', Mage::getModel('simplebundle/bundle_template')->load($this->getRequest()->getParam('templateId')));
        }
        return $this->getResponse()->setBody(
                        $this->getLayout()
                                ->createBlock('simplebundle/adminhtml_template_item_grid', 'template.popup.item.grid')
                                ->toHtml()
        );
    }
    
    public function productgridAction() {
        return $this->getResponse()->setBody(
                        $this->getLayout()
                                ->createBlock('simplebundle/adminhtml_template_edit_tab_bundle_popup_product_grid', 'template.popup.add.product.grid')
                                ->toHtml()
        );
    }
    
    
    public function savetemplateitemAction() {
        $templateItemId = $this->getRequest()->getParam('templateItemId');
        $templateId = $this->getRequest()->getParam('templateId');
        $productId = $this->getRequest()->getParam('productId');
        $selectionQty = intval($this->getRequest()->getParam('selectionQty'));
        $position = $this->getRequest()->getParam('position');
        $delete = $this->getRequest()->getParam('delete');
        
        if ($productId && $selectionQty > 0 && $templateId) {
            $templateItem = Mage::getModel('simplebundle/bundle_template_item');
            if($templateItemId) {
                $templateItem->load($templateItemId);
                if($delete) {
                    $templateItem->delete();
                    return;
                }
                if($templateItem->getTemplateId() != $templateId) {
                    throw new Exception('Item does not correspond to this customer.');
                }
            } else {
                if($delete) {
                    return;
                }
                $templateItem->setData('template_id', $templateId);
                $templateItem->setData('product_id', $productId);
            }
            $templateItem->setData('selection_qty', $selectionQty);
            $templateItem->setData('position', $position);
            
            $templateItem->save();
        }
    }
}
