<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Model_Bundle_Template extends Mage_Core_Model_Abstract {

    protected $_selectionProducts = null;
    protected $_bundleUpdateFields = array(
        'active' => 'active',
        'stores' => 'stores',
        'position' => 'position',
        'discount_amount' => 'discount_amount',
        'discount_type' => 'discount_type',
        'base_qty' => 'base_qty',
        'exclude_base_product_from_discount' => 'exclude_base_product_from_discount',
        'special_price_behavior' => 'special_price_behavior',
        'bundle_name' => 'bundle_name');
    protected $_itemUpdateFields = array('product_id', 'selection_qty', 'position');
    protected $_rule = null;

    public function _construct() {
        parent::_construct();
        $this->_init('simplebundle/bundle_template');
    }

    protected function _getItemKey($item) {
        $key = '';
        foreach ($this->_itemUpdateFields as $field) {
            $key .= $item->getData($field) . '%%';
        }
        return $key;
    }

    public function updateBundle(Webcooking_SimpleBundle_Model_Bundle $bundle) {
        if (!$this->getId()) {
            return;
        }
        $shouldSaveBundle = false;
        foreach ($this->_bundleUpdateFields as $templateField => $bundleField) {
            $bundle->setData($bundleField, $this->getData($templateField));
            if ($bundle->dataHasChangedFor($bundleField)) {
                $shouldSaveBundle = true;
            }
        }
        if ($shouldSaveBundle) {
            $bundle->setOrigData('template_id', $bundle->getTemplateId());
            $bundle->save();
        }
        $bundleItemCollection = Mage::getModel('simplebundle/bundle_item')->getCollection();
        $bundleItemCollection->addFieldToFilter('simple_bundle_id', $bundle->getId());
        $bundleItems = array();
        foreach ($bundleItemCollection as $bundleItem) {
            $key = $this->_getItemKey($bundleItem);
            $bundleItems[$key] = $bundleItem;
        }
        $templateItemcollection = Mage::getModel('simplebundle/bundle_template_item')->getCollection();
        $templateItemcollection->addFieldToFilter('template_id', $this->getId());
        foreach ($templateItemcollection as $templateItem) {
            $key = $this->_getItemKey($templateItem);
            if (isset($bundleItems[$key])) {
                //update item
                foreach ($this->_itemUpdateFields as $field) {
                    $bundleItems[$key]->setData($field, $templateItem->getData($field));
                    if ($bundleItems[$key]->dataHasChangedFor($field)) {
                        $bundleItems[$key]->setShouldSave(true);
                    }
                }
                $bundleItems[$key]->setFoundInTemplate(true);
            } else {
                $newBundleItem = Mage::getModel('simplebundle/bundle_item');
                $newBundleItem->setSimpleBundleId($bundle->getId());
                $newBundleItem->setProductId($templateItem->getProductId());
                $newBundleItem->setSelectionQty($templateItem->getSelectionQty());
                $newBundleItem->setPosition($templateItem->getPosition());
                $newBundleItem->setShouldSave(true);
                $newBundleItem->setFoundInTemplate(true);
                $key = $this->_getItemKey($templateItem);
                $bundleItems[$key] = $newBundleItem;
            }
        }
        foreach ($bundleItems as $bundleItem) {
            if ($bundleItem->getShouldSave()) {
                $bundleItem->save();
            } else if (!$bundleItem->getFoundInTemplate()) {
                $bundleItem->delete();
            }
        }
    }

    protected function _afterSave() {
        $this->updateLinkedBundles();
        return parent::_afterSave();
    }

    public function updateLinkedBundles() {
        $isSelectionMode = false;
        if ($this->getData('selected_products')) {
            $isSelectionMode = true;
            $selectedProducts = Mage::helper('adminhtml/js')->decodeGridSerializedInput($this->getData('selected_products'));
            $bundleCollection = Mage::getModel('simplebundle/bundle')->getCollection();
            $bundleCollection->addFieldToFilter('template_id', $this->getId());
            $bundleCollection->load();
            foreach ($selectedProducts as $productId) {
                $bundleExist = false;
                foreach ($bundleCollection as $bundle) {
                    if ($bundle->getProductId() == $productId) {
                        $bundleExist = true;
                    }
                }
                if (!$bundleExist) {
                    $bundle = Mage::getModel('simplebundle/bundle');
                    $bundle->setProductId($productId);
                    $bundle->setTemplateId($this->getId());
                    $bundle->save();
                }
            }
        }
        $bundleCollection = Mage::getModel('simplebundle/bundle')->getCollection();
        $bundleCollection->addFieldToFilter('template_id', $this->getId());
        foreach ($bundleCollection as $bundle) {
            if ($isSelectionMode && !in_array($bundle->getProductId(), $selectedProducts)) {
                $bundle->delete();
            } else {
                $this->updateBundle($bundle);
            }
        }
    }

    public function getRule() {
        if (is_null($this->_rule)) {
            $this->_rule = Mage::getModel('simplebundle/bundle_template_rule');
            $this->_rule->setConditionsSerialized($this->getConditionsSerialized());
        }
        return $this->_rule;
    }

    public function getProductCollectionMatchingRule($addHasTemplate=false, $productCollection = false) {
        $rule = $this->getRule();
        if(!$productCollection) {
            $productCollection = new Webcooking_All_Model_Resource_Catalog_Product_Collection();
        }
        $rule->applyConditionToProductCollection($productCollection);
        if($addHasTemplate) {
            $productCollection->getSelect()
                ->joinLeft(
                        array('sb' => $productCollection->getTable('simplebundle/bundle')),
                        'sb.product_id = e.entity_id and sb.template_id = ' . $this->getTemplateId(),
                        array('has_template'=>new Zend_Db_Expr('if(template_id, 1, 0)'))
                );
        }
        return $productCollection;
    }

    
    public function applyRule() {
        $productCollection = $this->getProductCollectionMatchingRule(true);
        $productCollection->getSelect()->where('sb.template_id IS NULL');
        foreach($productCollection as $product) {
            $bundle = Mage::getModel('simplebundle/bundle');
            $bundle->setProductId($product->getId());
            $bundle->setTemplateId($this->getId());
            $bundle->save();
        }
    }
    

}
