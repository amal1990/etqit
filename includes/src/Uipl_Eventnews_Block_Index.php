<?php   
class Uipl_Eventnews_Block_Index extends Mage_Core_Block_Template{   

    private $_limit;
    private $_page;
    private $_total;
    public function __construct()
    {
        parent::__construct();
        $collection = Mage::getModel('eventnews/eventnews')->getCollection();
        $this->setCollection($collection);
        $this->_limit=5;
        $this->_page=1;
        $this->_total=$collection->count();
    }


    protected function _prepareLayout()
    {
        parent::_prepareLayout();
 
        //$pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        //$pager->setAvailableLimit(array(2=>2,3=>3,20=>20,'all'=>'all'));
        //$pager->setCollection($this->getCollection());
        //$this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }
    
    public function getCollection()     
    {            
        $this->_page    =    1;

        if(Mage::app()->getRequest()->getParam('page'))
        {
            $this->_page    =    Mage::app()->getRequest()->getParam('page');
        }

        //Calculate Offset    
        $offset     =    ($this->_page - 1) * $this->_limit;

        $collection    =    Mage::getModel('eventnews/eventnews')->getCollection()
                                                    ->setOrder('pubdate','DESC');

        $collection->getSelect()->limit($this->_limit,$offset);

        return $collection;
    }  
 
    //public function getPagerHtml()
    //{
    //    return $this->getChildHtml('pager');
    //}
    
    
    public function resizeImg($fileName, $width, $height = '')
    {
        $folderURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $imageURL = $folderURL . $fileName;
     
        $basePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . $fileName;
        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . "resized" . DS . $fileName;
        //if width empty then return original size image's URL
        if ($width != '') {
            //if image has already resized then just return URL
            if (file_exists($basePath) && is_file($basePath) && !file_exists($newPath)) {
                $imageObj = new Varien_Image($basePath);
                $imageObj->constrainOnly(TRUE);
                $imageObj->keepAspectRatio(FALSE);
                $imageObj->keepFrame(FALSE);
                $imageObj->resize($width, $height);
                $imageObj->save($newPath);
            }
            $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "resized" . DS . $fileName;
         } else {
            $resizedURL = $imageURL;
         }
         return $resizedURL;
    }
    
    
    public function resizeThumbImg($fileName, $width, $height = '')
    {
        $folderURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
        $imageURL = $folderURL . $fileName;
     
        $basePath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . $fileName;
        $newPath = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . "resized" . DS ."newsThumbimage".DS. $fileName;
        //if width empty then return original size image's URL
        if ($width != '') {
            //if image has already resized then just return URL
            if (file_exists($basePath) && is_file($basePath) && !file_exists($newPath)) {
                $imageObj = new Varien_Image($basePath);
                $imageObj->constrainOnly(TRUE);
                $imageObj->keepAspectRatio(FALSE);
                $imageObj->keepFrame(FALSE);
                $imageObj->resize($width, $height);
                $imageObj->save($newPath);
            }
            $resizedURL = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . "resized" . DS ."newsThumbimage".DS. $fileName;
         } else {
            $resizedURL = $imageURL;
         }
         return $resizedURL;
    }
    
    function createLinks()
    {
        $current_page=$this->_page;
        $total_records=$this->_total;
        $total_pages=ceil( $this->_total / $this->_limit );
        $pagination = '';
        if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
            $pagination .= '<ul class="pagination">';
            
            $right_links    = $current_page + 3; 
            $previous       = $current_page - 1; //previous link 
            $next           = $current_page + 1; //next link
            $first_link     = true; //boolean var to decide our first link
            
            if($current_page > 1){
                $previous_link = ($previous==0)?1:$previous;
                $pagination .= '<li class="previous"><a href="?page=1" title="First"><i class="fa fa-angle-double-left"></i></a>'; //first link
                $pagination .= '<a href="?page='.$previous_link.'" title="Previous"><i class="fa fa-angle-left"></i></a></li>'; //previous link
                    for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                        if($i > 0){
                            $pagination .= '<li><a href="?page='.$i.'">'.$i.'</a></li>';
                        }
                    }   
                $first_link = false; //set first link to false
            }
            
            if($first_link){ //if current active page is first link
                $pagination .= '<li class="first active">'.$current_page.'</li>';
            }elseif($current_page == $total_pages){ //if it's the last active link
                $pagination .= '<li class="last active">'.$current_page.'</li>';
            }else{ //regular current link
                $pagination .= '<li class="active">'.$current_page.'</li>';
            }
                    
            for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
                if($i<=$total_pages){
                    $pagination .= '<li><a href="?page='.$i.'">'.$i.'</a></li>';
                }
            }
            if($current_page < $total_pages){ 
                    //$next_link = ($i > $total_pages)? $total_pages : $i;
                    $pagination .= '<li class="next"><a href="?page='.$next.'" title="Next"><i class="fa fa-angle-right"></i></a>'; //next link
                    $pagination .= '<a href="?page='.$total_pages.'" title="Last"><i class="fa fa-angle-double-right"></i></a></li>'; //last link
            }
            
            $pagination .= '</ul>'; 
        }
        return $pagination; //return pagination links
    }
    
    //public function createLinks( $links, $list_class )
    //{
    //    if ( $this->_limit == 'all' ) {
    //        return '';
    //    }
    //
    //    $last       = ceil( $this->_total / $this->_limit );
    // 
    //    $start      = ( ( $this->_page - $links ) > 0 ) ? $this->_page - $links : 1;
    //    $end        = ( ( $this->_page + $links ) < $last ) ? $this->_page + $links : $last;
    // 
    //    $html       = '<ul class="' . $list_class . '">';
    // 
    //    $class      = ( $this->_page == 1 ) ? "disabled" : "";
    //    $html       .= '<li class="' . $class . '"><a href="?page=' . ( $this->_page - 1 ) . '"><i class="fa fa-angle-double-left"></i></a></li>';
    // 
    //    if ( $start > 1 ) {
    //        $html   .= '<li><a href="?page=1">1</a></li>';
    //        $html   .= '<li class="disabled"><span>...</span></li>';
    //    }
    // 
    //    for ( $i = $start ; $i <= $end; $i++ ) {
    //        $class  = ( $this->_page == $i ) ? "active" : "";
    //        $html   .= '<li class="' . $class . '"><a href="?page=' . $i . '">' . $i . '</a></li>';
    //    }
    // 
    //    if ( $end < $last ) {
    //        $html   .= '<li class="disabled"><span>...</span></li>';
    //        $html   .= '<li><a href="?page=' . $last . '">' . $last . '</a></li>';
    //    }
    // 
    //    $class      = ( $this->_page == $last ) ? "disabled" : "";
    //    $html       .= '<li class="' . $class . '"><a href="?page=' . ( $this->_page + 1 ) . '"><i class="fa fa-angle-double-right"></i></a></li>';
    // 
    //    $html       .= '</ul>';
    // 
    //    return $html;
    //}
    
    public function newslist()
    {
        $collection = Mage::getModel('eventnews/eventnews')->getCollection()->setPageSize(3)->setOrder('pubdate','DESC');
        return $collection;
    }

}