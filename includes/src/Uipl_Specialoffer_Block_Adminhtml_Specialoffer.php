<?php


class Uipl_Specialoffer_Block_Adminhtml_Specialoffer extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_specialoffer";
	$this->_blockGroup = "specialoffer";
	$this->_headerText = Mage::helper("specialoffer")->__("Specialoffer Manager");
	$this->_addButtonLabel = Mage::helper("specialoffer")->__("Add New Item");
	parent::__construct();
	
	}

}