<?php
class Uipl_Specialoffer_Block_Adminhtml_Specialoffer_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("specialoffer_form", array("legend"=>Mage::helper("specialoffer")->__("Item information")));

								
						$fieldset->addField('image', 'image', array(
						'label' => Mage::helper('specialoffer')->__('Image'),
						'name' => 'image',
						'note' => '(*.jpg, *.png, *.gif)',
						));
						$fieldset->addField("url", "text", array(
						"label" => Mage::helper("specialoffer")->__("Url"),					
						"class" => "required-entry",
						"required" => true,
						"name" => "url",
						));
									
						 $fieldset->addField('status', 'select', array(
						'label'     => Mage::helper('specialoffer')->__('Status'),
						'values'   => Uipl_Specialoffer_Block_Adminhtml_Specialoffer_Grid::getValueArray3(),
						'name' => 'status',
						));

				if (Mage::getSingleton("adminhtml/session")->getSpecialofferData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getSpecialofferData());
					Mage::getSingleton("adminhtml/session")->setSpecialofferData(null);
				} 
				elseif(Mage::registry("specialoffer_data")) {
				    $form->setValues(Mage::registry("specialoffer_data")->getData());
				}
				return parent::_prepareForm();
		}
}
