<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */

class Webcooking_SimpleBundle_Block_Adminhtml_Template_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        $this->_objectId = 'template_id';
        $this->_blockGroup = 'simplebundle';
        $this->_controller = 'adminhtml_template';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('simplebundle')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('simplebundle')->__('Delete'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        //todo : put this in an extern js..
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
            function toggleElements() {
                if($('template_all_stores').getValue() == 1) {
                    $('template_stores').up('tr').hide();
                } else {
                    $('template_stores').up('tr').show();
                }
            }
            toggleElements();
            $('template_all_stores').observe('change', toggleElements);
        ";
    }

    public function getHeaderText()
    {
        if (Mage::registry('simplebundle_template')->getId()) {
            return Mage::helper('simplebundle')->__("Edit template '%s'", $this->htmlEscape(Mage::registry('simplebundle_template')->getName()));
        }
        else {
            return Mage::helper('simplebundle')->__('New template');
        }
    }
    
    public function getFormActionUrl()
    {
        return $this->getUrl('adminhtml/simplebundle_template/save');
    }

}