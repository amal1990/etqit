<?php

class Uipl_Specialoffer_Block_Adminhtml_Specialoffer_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("specialofferGrid");
				$this->setDefaultSort("specialoffer_id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("specialoffer/specialoffer")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("specialoffer_id", array(
				"header" => Mage::helper("specialoffer")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "specialoffer_id",
				));
                
				$this->addColumn("url", array(
				"header" => Mage::helper("specialoffer")->__("Url"),
				"index" => "url",
				));
						$this->addColumn('status', array(
						'header' => Mage::helper('specialoffer')->__('Status'),
						'index' => 'status',
						'type' => 'options',
						'options'=>Uipl_Specialoffer_Block_Adminhtml_Specialoffer_Grid::getOptionArray3(),				
						));
						
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV')); 
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('specialoffer_id');
			$this->getMassactionBlock()->setFormFieldName('specialoffer_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_specialoffer', array(
					 'label'=> Mage::helper('specialoffer')->__('Remove Specialoffer'),
					 'url'  => $this->getUrl('*/adminhtml_specialoffer/massRemove'),
					 'confirm' => Mage::helper('specialoffer')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray3()
		{
            $data_array=array(); 
			$data_array[1]='Yes';
			$data_array[0]='No';
            return($data_array);
		}
		static public function getValueArray3()
		{
            $data_array=array();
			foreach(Uipl_Specialoffer_Block_Adminhtml_Specialoffer_Grid::getOptionArray3() as $k=>$v){
               $data_array[]=array('value'=>$k,'label'=>$v);		
			}
            return($data_array);

		}
		

}