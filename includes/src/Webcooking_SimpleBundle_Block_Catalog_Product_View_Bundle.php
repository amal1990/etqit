<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Block_Catalog_Product_View_Bundle extends Mage_Core_Block_Template
{
    
    
    
   public function getProduct() {
       if(!$this->getData('product'))
           $this->setData('product', Mage::registry('product') ? Mage::registry('product') : Mage::registry('current_product'));
       return $this->getData('product');
   }
    
   public function getBundles() {
       if(!$this->getProduct()) {
           return array();
       }
       return Mage::helper('simplebundle')->getBundleCollectionForProduct($this->getProduct(), $this->getLimit());
   }
   
   public function getAddToCartUrl($bundle) {
       return $this->getUrl('simplebundle/cart/add', array('selection'=>base64_encode(serialize($bundle->getId()))));
   }
   
   public function getUpdatePricesUrl() {
       return $this->getUrl('simplebundle/cart/priceupdate');
   }
   
   public function getPopupAjaxUrl($bundle) {
       return $this->getUrl('simplebundle/cart/addtocartpopup', array('bundle_id'=>$bundle->getId()));
   }
   
   public function getAddToCartViaPopupUrl($bundle) {
       return $this->getUrl('simplebundle/cart/addfrompopup', array('bundle_id'=>$bundle->getId()));
   }
  
   
   protected function _toHtml() {
       if(!$this->getProduct()) {
           return '';
       }
       return parent::_toHtml();
   }
   
   
}