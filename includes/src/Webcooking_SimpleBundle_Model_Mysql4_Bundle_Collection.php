<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Model_Mysql4_Bundle_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    protected $_skipSalableCheck = false;
    protected $_skipPromoCheck = false;

    public function _construct() {
        parent::_construct();
        $this->_init('simplebundle/bundle');
        $this->setOrder('position', 'ASC');
    }

    public function skipSalableCheck() {
        $this->_skipSalableCheck = true;
        return $this;
    }

    public function skipPromoCheck() {
        $this->_skipPromoCheck = true;
        return $this;
    }

    public function skipAllChecks() {
        return $this
                ->skipPromoCheck()
                ->skipSalableCheck();
    }
    
    
    public function addProductIdsFilter($productsIds) {
        if(empty($productsIds)) return $this;
        $this->addFieldToFilter('product_id', $productsIds);
        return $this;
    }
    
    public function joinMasterProduct() {
        $productNameAttributeId = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'name')->getId();

        $this->getSelect()
                ->joinLeft(
                        array('sku_table' => Mage::getConfig()->getTablePrefix() . 'catalog_product_entity'), 'sku_table.entity_id=product_id', array('sku' => 'sku')
                );
        
        $this->getSelect()
                ->joinLeft(
                        array('name_table' => Mage::getConfig()->getTablePrefix() . 'catalog_product_entity_varchar'), 'name_table.entity_id=product_id and name_table.attribute_id = ' . $productNameAttributeId, array('name' => 'value')
                );
        
        $this->getSelect()->group('simple_bundle_id');
        
        return $this;

    }
    
    public function getSize() {
        if (is_null($this->_totalRecords)) {
            $sql = $this->getSelectCountSql();

            $result = $this->getConnection()->fetchAll($sql, $this->_bindParams);
            

            foreach ($result as $row) {
                $this->_totalRecords += reset($row);
            }
        }
        return intval($this->_totalRecords);
    }
    
    
    
     public function appendSelections($withAttributes=true) {
        $withAttributes = !$this->_skipSalableCheck || $withAttributes;
        $bundleToRemove = array();
        foreach ($this as $bundle) {
            
            $bundleIsValidated = true;
            
            $bundle->loadSelections($withAttributes);
            if(!$this->_skipPromoCheck) {
                if($bundle->getSpecialPriceBehavior() == Webcooking_SimpleBundle_Helper_Data::SPECIAL_PRICE_BEHAVIOR_DISABLE && $bundle->hasSpecialPrice()) {
                    $bundleIsValidated = false;
                }
            }
            
            if($bundleIsValidated && !$this->_skipSalableCheck) {
                $bundleIsValidated = $bundle->isSalable();
            }
            
            if ($bundleIsValidated) {
                $bundle->setSelections($bundle->getSelections());
            } else {
                $bundleToRemove[] = $bundle->getId();
            }
        }
        
        
        foreach ($bundleToRemove as $unsaleableBundle) {
            $this->removeItemByKey($unsaleableBundle);
        }
        
        return $this;
    }
    

    public function setAddedIds($bundleIds) {
        if (Mage::getStoreConfig('simplebundle/general/always_discount')) {
            if (empty($bundleIds)) {
                return $this;
            }
            $this->unshiftOrder(new Zend_Db_Expr('IF(simple_bundle_id in (' . implode(',', $bundleIds) . '),1,0)'), 'DESC');
        } else {
            $this->addFieldToFilter('simple_bundle_id', array('in' => $bundleIds));
        }
        return $this;
    }
    
    public function addStoreFilter($storeId=false) {
        if(!$storeId) {
            $storeId = Mage::app()->getStore()->getId();
        }
        $this->addFieldToFilter('stores', array(
            array('finset'=>$storeId),
            array('eq'=>0)));
        
        return $this;
    }

}