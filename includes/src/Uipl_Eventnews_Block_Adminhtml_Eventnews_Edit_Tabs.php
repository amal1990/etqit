<?php
class Uipl_Eventnews_Block_Adminhtml_Eventnews_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("eventnews_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("eventnews")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("eventnews")->__("Item Information"),
				"title" => Mage::helper("eventnews")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("eventnews/adminhtml_eventnews_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
