<?php
ini_set('memory_limit', '2548M');
set_time_limit(0);  
ini_set('display_errors', true);
error_reporting(-1);

$wsdl = 'http://mage18.web-cooking.net/index.php/api/v2_soap?wsdl=1';

$client = new SoapClient($wsdl);

$sessionId = $client->login('test', '123456');


$cartId = $client->shoppingCartCreate($sessionId, 1);
echo "CART ".$cartId."\n";

$result = $client->simplebundleAddtocart($sessionId, $cartId, 34, '', 1);
if($result) {
    echo "BUNDLE ADDED\n";
}

$cartInfo = $client->shoppingCartInfo($sessionId, $cartId);
echo $cartInfo->grand_total . '   '.(count($cartInfo->items))." items in cart\n";

echo "\n";


$additionalData = array(
    'sbdata' => array(2/*subproduct ID*/ => array(
            'options'=>array('32'=>'OPTION VALUE 1','31'=>'OPTION VALUE 2')
        )
    ));
echo json_encode($additionalData)."\n";
$result = $client->simplebundleAddtocart($sessionId, $cartId, 40, json_encode($additionalData), 1);
if($result) {
    echo "COMPLEX BUNDLE ADDED\n";
}

$items = $client->shoppingCartProductList($sessionId, $cartId);
var_dump($items);

echo "\n";

$client->endSession($sessionId);