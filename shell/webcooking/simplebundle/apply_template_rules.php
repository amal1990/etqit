<?php
ini_set('memory_limit', '2548M');
set_time_limit(0);  
ini_set('display_errors', true);
error_reporting(-1);
require_once("../../../app/Mage.php");
Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);


$cron = Mage::getModel('simplebundle/cron');
$cron->applyTemplateRules();