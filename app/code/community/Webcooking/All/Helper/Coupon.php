<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_All
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_All_Helper_Coupon extends Mage_Core_Helper_Abstract {

   
    
    public static function getRuleByCoupon($couponCode) {
       $oCoupon = Mage::getModel('salesrule/coupon')->load($couponCode, 'code');
       if(!$oCoupon->getId()) {
           return false;
       }
       $oRule = Mage::getModel('salesrule/rule')->load($oCoupon->getRuleId());
       if(!$oRule->getId()) {
           return false;
       }
       return $oRule;
    }
    
    
    public static function couponExists($couponCode) {
        if(version_compare(Mage::getVersion(),'1.4.0.0') >= 0) {
            $couponCollection = Mage::getModel('salesrule/coupon')->getCollection();
            $couponCollection->getSelect()->where('code = ?', $couponCode);
            $couponCollection->load();
            return ($couponCollection->getSize()!=0);
        } else {
            return true;
        }
    }

    /**
     * This function creates a gift coupon. It does NOT implement all of the magento coupon functionnalities. 
     * 
     * @param $name               Name of the coupon
     * @param $amount            Amount of the coupon
     * @param $action             Action of the coupon, can be by_percent, by_fixed, cart_fixed. Warning, buy_x_get_y is not managed yet by this function. Default is cart_fixed
     * @param $minAmount	  Minimum amount of the subtotal to make the coupon usable
     * @param $freeShipping	  Free shipping...
     * @param $couponCode	  Code of coupon, by default : uniqid()
     * @param $description	  Description of the coupon
     * @param $toDate		  End date of the coupon
     * @param $fromDate		  Start date of the coupon
     * @param $groupsIds	  Customer Groups Ids which can use the coupon (all by default)
     * @param $websitesIds	  Websites Ids where the coupon can be use (all by default)
     * @param $usesPerCoupon	  Uses per coupon (1 by default)
     * @param $usesPerCustomer	  Uses per customer (1 by default)
     * @return string             Coupon code
     */
    public function createCoupon($name, $amount, $action='cart_fixed', $minAmount = -1, $freeShipping = '0', $couponCode = '', $description = '', $toDate = null, $fromDate = null, $groupsIds = array(), $websitesIds=array(), $usesPerCoupon = '1', $usesPerCustomer = '1', $storeLabels = false) {
        if (!$couponCode)
            $couponCode = uniqid();
        else
            $couponCode = Mage::helper('wcooall')->applyReplaceAccent($couponCode);

        $couponCode = preg_replace('%[^a-zA-Z0-9-]%i', '-', $couponCode);
        $couponCode = preg_replace('%--+%i', '-', $couponCode);
        if(self::couponExists($couponCode)) {
            return false;
        }
        
        if (!$description)
            $description = $name;
        
        if(!$storeLabels) {
            $storeLabels = array($name);
        }

        if (!$fromDate)
            $fromDate = date('Y-m-d');

        if (!$toDate)
            $toDate = date('Y-m-d', mktime(0, 0, 0, date('m') + 1, date('d'), date('y')));

        if (!in_array($action, array('cart_fixed', 'by_fixed', 'by_percent')))
            $action = 'cart_fixed';

        if (!is_array($groupsIds) || empty($groupsIds))
            $groupsIds = implode(',', Mage::helper('wcooall/customer')->getAllGroupsId());
        else
            $groupsIds = implode(',', $groupsIds);


        if (!is_array($websitesIds) || empty($websitesIds))
            $websitesIds = implode(',', Mage::helper('wcooall/website')->getAllWebsitesId());
        else
            $websitesIds = implode(',', $websitesIds);

        
            
            //Create rule
            $rule = Mage::getModel('salesrule/rule');
            $rule->setRuleId(null)
                    ->setName($name)
                    ->setDescription($description)
                    ->setFromDate($fromDate)
                    ->setToDate($toDate)
                    ->setCustomerGroupIds($groupsIds)
                    ->setIsActive('1')
                    ->setStopRulesProcessing('0')
                    ->setIsAdvanced('1')
                    ->setSimpleAction($action)
                    ->setDiscountAmount($amount)
                    ->setDiscountQty('')
                    ->setDiscountStep('0')
                    ->setSimpleFreeShipping($freeShipping)
                    ->setTimesUsed('0')
                    ->setIsRss('0')
                    ->setUsesPerCustomer($usesPerCustomer)
                    ->setCouponType('2')
                    ->setWebsiteIds($websitesIds);
        if(version_compare(Mage::getVersion(),'1.4.0.0') < 0) {
                    $rule
                    ->setCouponCode($couponCode)
                    ->setUsesPerCoupon($usesPerCoupon);
        }
       
        if ($minAmount != -1) {
            $conditions = Mage::getModel('salesrule/rule_condition_combine');
            $conditions->setRule($rule)->setId('1')->setPrefix('conditions');
            $conditions->loadArray(array
                (
                'type' => 'salesrule/rule_condition_combine',
                'attribute' => '',
                'operator' => '',
                'value' => '1',
                'is_value_processed' => '',
                'aggregator' => 'all',
                'conditions' => array
                    (
                    array
                        (
                        'type' => 'salesrule/rule_condition_address',
                        'attribute' => 'base_subtotal',
                        'operator' => '>=',
                        'value' => (string)$minAmount,
                        'is_value_processed' => ''
                    )
                )
                    )
            );
            $rule->setConditions($conditions);
        }

        try {
            $rule->setStoreLabels($storeLabels)->save();
            if(version_compare(Mage::getVersion(),'1.4.0.0') >= 0) {
                $coupon = Mage::getModel('salesrule/coupon');
                $coupon->setRuleId($rule->getId())
                       ->setCode($couponCode)
                       ->setUsageLimit($usesPerCoupon)
                       ->setUsagePerCustomer($usesPerCustomer)
                       ->setExpirationDate($toDate)
                       ->setIsPrimary(1)
                       ->save();
            }
        } catch (Exception $e) {
            Mage::logException($e);
            return false;
        }
        return $couponCode;
    }
}