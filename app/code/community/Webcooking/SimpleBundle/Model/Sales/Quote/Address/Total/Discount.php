<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Model_Sales_Quote_Address_Total_Discount extends Mage_Sales_Model_Quote_Address_Total_Abstract {

    
    public function collect(Mage_Sales_Model_Quote_Address $address) {
        parent::collect($address);
   //we can return if there are no items
        $items = $address->getAllItems();
        if (!count($items)) {
            return $this;
        }

        $quote = $address->getQuote();
        $quoteId = $quote->getId();
        if (!is_numeric($quoteId)) {
            return $this;
        }


        if (($address->getAddressType() == 'billing' && !$quote->isVirtual())) {
            return $this;
        }

        foreach($quote->getAllItems() as $item) {
            if($item->getSimpleBundleDiscountAmount() > 0 /*&& floatval($item->getWeeeTaxAppliedAmount()) <= 0*/ ) {
                $item->setDiscountAmount($item->getDiscountAmount() + $quote->getStore()->roundPrice($item->getSimpleBundleDiscountAmount() - $item->getSimpleBundleDiscountTaxAmount()));
                $item->setBaseDiscountAmount($item->getBaseDiscountAmount() + $quote->getStore()->roundPrice($item->getBaseSimpleBundleDiscountAmount() - $item->getBaseSimpleBundleDiscountTaxAmount()));

                $item->setTaxAmount($item->getTaxAmount() - $item->getSimpleBundleDiscountTaxAmount());
                $item->setBaseTaxAmount($item->getBaseTaxAmount() - $item->getBaseSimpleBundleDiscountTaxAmount());

                $item->setHiddenTaxAmount($item->getHiddenTaxAmount() - ($item->getSimpleBundleDiscountTaxAmount() - $quote->getStore()->roundPrice($item->getSimpleBundleDiscountTaxAmount())));
                $item->setBaseHiddenTaxAmount($item->getBaseHiddenTaxAmount() - ($item->getBaseSimpleBundleDiscountTaxAmount() - $quote->getStore()->roundPrice($item->getBaseSimpleBundleDiscountTaxAmount())));
            }
        }
        
        
        
        
        return $this;
    }

    public function fetch(Mage_Sales_Model_Quote_Address $address) {
        return $this;
    }
    
    

    
}
