<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Model_Sales_Quote_Address_Total_Sbundle extends Mage_Sales_Model_Quote_Address_Total_Abstract {

    protected $_baseSimpleBundleAmount = 0;
    protected $_simpleBundleAmount = 0;
    protected $_baseSimpleBundleTaxAmount = 0;
    protected $_simpleBundleTaxAmount = 0;
    protected $_simpleBundleTaxDetails = array();
    protected $_baseSimpleBundleTaxDetails = array();
    protected $_simpleBundleDescription = '';

    public function getLabel() {
        return Mage::helper('simplebundle')->__('Bundles discount amount');
    }

    public function collect(Mage_Sales_Model_Quote_Address $address) {
        parent::collect($address);
        
        $this->_baseSimpleBundleAmount = 0;
        $this->_simpleBundleAmount = 0;
        $this->_baseSimpleBundleTaxAmount = 0;
        $this->_simpleBundleTaxAmount = 0;
        $this->_simpleBundleTaxDetails = array();
        $this->_baseSimpleBundleTaxDetails = array();
        $this->_simpleBundleDescription = '';
        //reset
        $address->setSbundleDiscountAmount(0);
        $address->setBaseSbundleDiscountAmount(0);



        //we can return if there are no items
        $items = $address->getAllItems();
        if (!count($items)) {
            return $this;
        }

        $quote = $address->getQuote();
        $quoteId = $quote->getId();
        if (!is_numeric($quoteId)) {
            return $this;
        }


        if (($address->getAddressType() == 'billing' && !$quote->isVirtual())) {
            return $this;
        }

        //Mage::log($quote->getId().' Collect', null, 'simplebundle.log');
        //Mage::log($quote->getId().' Previous bundleDiscount Amount : '.$quote->getSbundleDiscountAmount(), null, 'simplebundle.log');


        $quote->setSbundleDiscountAmount(0);
        $quote->setBaseSbundleDiscountAmount(0);
        $quote->setSbundleDiscountDescription(0);

        
    
        //get storeId
        $storeId = $quote->getStoreId();
        $this->process($quote);

        $bundleIdsMatched = Mage::getSingleton('checkout/session')->getSimpleBundleMatchedIds();
        if (!$bundleIdsMatched || empty($bundleIdsMatched)) {
            return $this;
        }
        

        $address->setSbundleDiscountAmount($this->_simpleBundleAmount);
        $address->setBaseSbundleDiscountAmount($this->_baseSimpleBundleAmount);
        $address->setSbundleDiscountTax($this->_simpleBundleTaxAmount);
        $address->setBaseSbundleDiscountTax($this->_baseSimpleBundleTaxAmount);

        $quote->setSbundleDiscountAmount($this->_simpleBundleAmount);
        $quote->setBaseSbundleDiscountAmount($this->_baseSimpleBundleAmount);
        $quote->setSbundleDiscountTax($this->_simpleBundleTaxAmount);
        $quote->setBaseSbundleDiscountTax($this->_baseSimpleBundleTaxAmount);
        $quote->setSbundleDiscountDescription($this->_simpleBundleDescription);
        
    
   

        if ($this->_simpleBundleAmount || $this->_baseSimpleBundleAmount) {
            $this->_setAmount(0 - $this->_simpleBundleAmount);
            $this->_setBaseAmount(0 - $this->_baseSimpleBundleAmount);
        }

        if (Mage::helper('weee')->isEnabled($quote->getStoreId()) && Mage::helper('weee')->includeInSubtotal($quote->getStoreId())) {
            // If FPT is included to subtotal, subtotal TTC comes wrong (subtotal ttc - bundletaxamount...)
            $taxAmount = $baseTaxAmount = 0;
            $subtotal = $baseSubtotal = 0;
            foreach($quote->getAllItems() as $item) {
                if ($item->getParentItem()) {
                    continue;
                }
                $taxAmount += $item->getTaxAmount() ;//- $item->getSimpleBundleDiscountTaxAmount();
                $baseTaxAmount += $item->getBaseTaxAmount() ;//- $item->getBaseSimpleBundleDiscountTaxAmount();
                $subtotal += $item->getRowTotal();
                $baseSubtotal += $item->getBaseRowTotal();
            }
            $address->setSubtotalInclTax($subtotal + $taxAmount);
            $address->setBaseSubtotalInclTax($baseSubtotal + $baseTaxAmount);
            $address->setBaseSubtotalTotalInclTax($baseSubtotal + $baseTaxAmount);
        }

      
        $address->setTaxAmount($address->getTaxAmount() - $this->_simpleBundleTaxAmount);
        $address->setBaseTaxAmount($address->getBaseTaxAmount() - $this->_baseSimpleBundleTaxAmount);
        $appliedTaxes = $address->getAppliedTaxes();
        foreach ($appliedTaxes as $key => $appliedTax) {
            $taxPercent = (float) $appliedTax['percent'];
            if (isset($this->_simpleBundleTaxDetails[$taxPercent])) {
                $appliedTaxes[$key]['amount'] -= $quote->getStore()->roundPrice($this->_simpleBundleTaxDetails[$taxPercent]);
                $appliedTaxes[$key]['base_amount'] -= $quote->getStore()->roundPrice($this->_baseSimpleBundleTaxDetails[$taxPercent]);
            }
        }
        $address->setAppliedTaxes($appliedTaxes);
        
        //Mage::log($quote->getId().' Sbundleamount : '.$this->_simpleBundleAmount, null, 'simplebundle.log');
        //Mage::log($quote->getId().' Sbundletaxamount : '.$this->_simpleBundleTaxAmount, null, 'simplebundle.log');
        //Mage::log($quote->getId().' End Collect', null, 'simplebundle.log');
        return $this;
    }

    protected $_fetched = false;

    public function fetch(Mage_Sales_Model_Quote_Address $address) {
        if (($address->getAddressType() == 'billing' && !$address->getQuote()->isVirtual())) {
            return $this;
        }
        $amount = $address->getSbundleDiscountAmount();
        if ($amount != 0) {
            $address->addTotal(array(
                'code' => $this->getCode(),
                'title' => $this->getLabel(),
                'value' => -$amount
            ));
        }
        return $this;
    }
    
    
    protected function _getScpCpid($quoteItem) {
        $cpid = false;
        if($quoteItem->getOptionByCode('cpid')) {
            $cpid = $quoteItem->getOptionByCode('cpid')->getValue();
        } else {
            $infoBuyRequest = @unserialize($quoteItem->getOptionByCode('info_buyRequest')->getData('value'));
            if(isset($infoBuyRequest['cpid'])) {
                $cpid = $infoBuyRequest['cpid'];
            }
        }
        return $cpid;
    }

    public function process($quote) {
        $quoteItems = $quote->getAllItems();
        
        foreach ($quoteItems as $quoteItem) {
            $quoteItem->setUsedInSimpleBundle(0);
            $quoteItem->setSimpleBundlePercentDiscount(0);
            $quoteItem->setSimpleBundleDiscountAmount(0);
            $quoteItem->setBaseSimpleBundleDiscountAmount(0);
            $quoteItem->setSimpleBundleDiscountTaxAmount(0);
            $quoteItem->setBaseSimpleBundleDiscountTaxAmount(0);
        }


        $bundleIdsAdded = Mage::getSingleton('checkout/session')->getSimpleBundleIds();
        
        if (!$bundleIdsAdded)
            $bundleIdsAdded = array();
        
        
        $bundleIdsMatched = array();

        $productIds = array();
        foreach($quote->getAllItems() as $_item) {
            $productIds[] = $_item->getProductId();
        }

        $bundleCollection = Mage::getModel('simplebundle/bundle')
                ->getCollection()
                ->skipAllChecks()
                ->addStoreFilter()
                ->addFieldToFilter('active', 1)
                ->setAddedIds($bundleIdsAdded)
                ->appendSelections(false);
        if(!Mage::helper('wcooall')->isModuleInstalled('OrganicInternet_SimpleConfigurableProducts')) {
            $bundleCollection->addProductIdsFilter($productIds);
        } 

        //we try to add at least one of each added bundles
        
        foreach($bundleIdsAdded as $bundleId) {
            foreach ($bundleCollection as $bundle) {
                if($bundle->getId() == $bundleId) {
                    if($this->_checkBundle($quote, $bundle, $quoteItems, 1)) {
                        $bundleIdsMatched[] = $bundle->getId();
                    } else {
                        $idx = array_search($bundle->getId(), $bundleIdsAdded);
                        if($idx !== false) {
                            unset($bundleIdsAdded[$idx]);
                        }
                    }
                }
            }
        }
        
        
        //we check for the rest
        foreach ($bundleCollection as $bundle) {
            do {
                $bundle->setHasMultipleMatches(false);
                $bundleMatched = $this->_checkBundle($quote, $bundle, $quoteItems);
                if($bundleMatched) {
                    $bundleIdsMatched[] = $bundle->getId();
                }
            } while($bundle->getHasMultipleMatches());
        }
        
        
        $bundleIdsMatched = array_unique($bundleIdsMatched);
        $bundleIdsMatched = array_filter($bundleIdsMatched);
        //refresh bundle added
        Mage::getSingleton('checkout/session')->setSimpleBundleIds($bundleIdsAdded);
        Mage::getSingleton('checkout/session')->setSimpleBundleMatchedIds($bundleIdsMatched);
       
        $quote->setSimpleBundleProcessed(true);
        return $this;
    }
    
    protected function _checkBundle($quote, $bundle, $quoteItems, $maxMachedQty=false) {
        $baseProductMatched = 0;
        $baseProductItem = null;
        $baseQty = $bundle->getBaseQty();

        //Check if base product matches
        foreach ($quoteItems as $quoteItem) {
            if ($quoteItem->getProductId() == $bundle->getProductId()) {
                $baseProductMatchedTemp = floor(($quoteItem->getQty() - $quoteItem->getUsedInSimpleBundle()) / ($baseQty<=0?1:$baseQty));
                if($baseProductMatchedTemp && $baseProductMatched) {
                    $bundle->setHasMultipleMatches(true);
                } else {
                    $baseProductMatched = $baseProductMatchedTemp;
                    $baseProductItem = $quoteItem;
                }
            } elseif (Mage::helper('wcooall')->isModuleInstalled('OrganicInternet_SimpleConfigurableProducts') && $this->_getScpCpid($quoteItem) == $bundle->getProductId()) {
                $baseProductMatchedTemp = floor(($quoteItem->getQty() - $quoteItem->getUsedInSimpleBundle()) / ($baseQty<=0?1:$baseQty));
                if($baseProductMatchedTemp && $baseProductMatched) {
                    $bundle->setHasMultipleMatches(true);
                } else {
                    $baseProductMatched = $baseProductMatchedTemp;
                    $baseProductItem = $quoteItem;
                }
            }
        }

        if ($baseProductMatched <= 0) {
            return false; // base Product is not in cart, so we stop here
        }

        //Check if selections matches
        $selectionMatches = array();
        $selectionPrices = array();
        $selectionMatch = true;
        foreach ($bundle->getSelections() as $selection) {
            $selectionProductMatched = 0;
            foreach ($quoteItems as $quoteItem) {
                if($selectionProductMatched) {
                    continue;
                }
                if ($quoteItem->getProductId() == $selection->getProductId()) {

                    $selectionProductMatched = floor(($quoteItem->getQty() - $quoteItem->getUsedInSimpleBundle()) / $selection->getSelectionQty());
                    if ($selectionProductMatched > 0) {
                        $selectionPrices[] = array(
                            'item' => $quoteItem,
                            'selection_qty' => $selection->getSelectionQty()
                        );
                    }
                } elseif (Mage::helper('wcooall')->isModuleInstalled('OrganicInternet_SimpleConfigurableProducts') && $this->_getScpCpid($quoteItem) == $selection->getProductId()) {
                    $selectionProductMatched = floor(($quoteItem->getQty() - $quoteItem->getUsedInSimpleBundle()) / $selection->getSelectionQty());
                    if ($selectionProductMatched > 0) {
                        $selectionPrices[] = array(
                            'item' => $quoteItem,
                            'selection_qty' => $selection->getSelectionQty()
                        );
                    }
                }
            }
            if ($selectionProductMatched <= 0) {
                $selectionMatch = false; // a selection Product is not in cart, so we stop here
            }
            $selectionMatches[] = $selectionProductMatched;
        }

        if (!$selectionMatch) {
            return false;
        }


        $selectionMatches[] = $baseProductMatched;
        $bundleMatchedQty = min($selectionMatches);
        if($maxMachedQty && $maxMachedQty < $bundleMatchedQty) {
            $bundleMatchedQty = $maxMachedQty;
        }

        if (!$bundle->getExcludeBaseProductFromDiscount()) {
            $selectionPrices[] = array(
                'item' => $baseProductItem,
                'selection_qty' => $bundle->getBaseQty()
            );
        } else {
            $baseProductItem->setUsedInSimpleBundle($baseProductItem->getUsedInSimpleBundle() + $bundle->getBaseQty() * $bundleMatchedQty);
        }

        $originalAmount = 0;
        if($bundle->getExcludeBaseProductFromDiscount()) {
            $originalAmount += ($this->_getQuoteItemPrice($baseProductItem, true) * $bundle->getBaseQty());
        }
        foreach ($selectionPrices as $selection) {
            $quoteItem = $selection['item'];
            $originalAmount += ($this->_getQuoteItemPrice($quoteItem, true) * $selection['selection_qty']);
        }



        $percentDiscount = $bundle->getDiscountPercentageValue(10, $originalAmount) / 100;
        $valueDiscount = $bundle->getDiscountAmountValue();
        


        $bundleDiscountAmount = 0;
        $bundleBaseDiscountAmount = 0;
        $bundleDiscountTaxAmount = 0;
        $bundleBaseDiscountTaxAmount = 0;

        foreach ($selectionPrices as $idx => $selection) {
            //Mark applyed redeem
            $quoteItem = $selection['item'];
            $quoteItem->setUsedInSimpleBundle($quoteItem->getUsedInSimpleBundle() + $selection['selection_qty'] * $bundleMatchedQty);
            $quoteItem->setSimpleBundlePercentDiscount($percentDiscount);

            //caculate total discount
            $itemPrice = $this->_getQuoteItemPrice($quoteItem);//$quoteItem->getPriceInclTax();
            $baseItemPrice = $this->_getQuoteItemPrice($quoteItem, true);//$quoteItem->getBasePriceInclTax();
             
            if(floatval($quoteItem->getWeeeTaxAppliedAmount()) > 0 && !Mage::getStoreConfigFlag('simplebundle/general/apply_discount_on_weee')) {
                $itemPrice -= $quoteItem->getWeeeTaxAppliedAmount();
                $baseItemPrice -= $quoteItem->getBaseWeeeTaxAppliedAmount();
            } 

            $itemDiscountAmount = $quote->getStore()->roundPrice($itemPrice * $selection['selection_qty'] * $bundleMatchedQty * $percentDiscount);
            $itemBaseDiscountAmount = $quote->getStore()->roundPrice($baseItemPrice * $selection['selection_qty'] * $bundleMatchedQty * $percentDiscount);

            
            if($idx == count($selectionPrices)-1 && abs($bundleDiscountAmount + $itemDiscountAmount - $valueDiscount) > 0 ) {
                $roundingAdjustment = $quote->getStore()->roundPrice($valueDiscount- ($bundleDiscountAmount + $itemDiscountAmount));
                if(abs($roundingAdjustment) > 1) {
                    //todo Should never happen. there is an error somewhere. Log ? Exception ?
                } else {
                    $itemDiscountAmount += $roundingAdjustment;
                }
            }
       
            
            $quoteItem->setSimpleBundleDiscountAmount($quoteItem->getSimpleBundleDiscountAmount() + $itemDiscountAmount);
            $quoteItem->setBaseSimpleBundleDiscountAmount($quoteItem->getBaseSimpleBundleDiscountAmount() + $itemBaseDiscountAmount);

            

            $bundleDiscountAmount += $itemDiscountAmount;
            $bundleBaseDiscountAmount += $itemBaseDiscountAmount;
            

            $quoteItemsToParse = array();
            if ($quoteItem->getProductType() == 'bundle') {
                foreach ($quoteItems as $quoteItem2) {
                    if ($quoteItem2->getParentItemId() == $quoteItem->getId()) {
                        $ratioBasePrice = $quoteItem->getBasePrice() / $quoteItem2->getBasePrice();
                        $ratioPrice = $quoteItem->getPrice() / $quoteItem2->getPrice();
                        $quoteItem2->setSimpleBundleDiscountAmount($quoteItem->getSimpleBundleDiscountAmount()/$ratioPrice);
                        $quoteItem2->setBaseSimpleBundleDiscountAmount($quoteItem->getBaseSimpleBundleDiscountAmount()/$ratioBasePrice);
                        $quoteItemClone = clone $quoteItem2;
                        $quoteItemClone->setQty($quoteItemClone->getQty() * $quoteItem->getQty());
                        $quoteItemsToParse[] = $quoteItemClone;
                    }
                }
            } else {
                $quoteItemsToParse = array($quoteItem);
            }

            foreach ($quoteItemsToParse as $quoteItemToParse) {
                $taxPercent = floatval($quoteItemToParse->getTaxPercent());
                if (!isset($this->_simpleBundleTaxDetails[$taxPercent])) {
                    $this->_simpleBundleTaxDetails[$taxPercent] = 0;
                }
                if (!isset($this->_baseSimpleBundleTaxDetails[$taxPercent])) {
                    $this->_baseSimpleBundleTaxDetails[$taxPercent] = 0;
                }

                $itemTaxAmount = $quoteItemToParse->getTaxAmount() + $quoteItemToParse->getHiddenTaxAmount();
                $baseItemTaxAmount = $quoteItemToParse->getBaseTaxAmount() + $quoteItemToParse->getBaseHiddenTaxAmount();
                $itemDiscountTaxAmount = (($itemTaxAmount / $quoteItemToParse->getQty()) * $selection['selection_qty'] * $bundleMatchedQty * $percentDiscount);
                $baseItemDiscountTaxAmount = (($baseItemTaxAmount / $quoteItemToParse->getQty()) * $selection['selection_qty'] * $bundleMatchedQty * $percentDiscount);

                $this->_simpleBundleTaxDetails[$taxPercent] += $itemDiscountTaxAmount;
                $this->_baseSimpleBundleTaxDetails[$taxPercent] += $baseItemDiscountTaxAmount;
                
                foreach ($quoteItems as $quoteItem2) {
                    if($quoteItem2->getProductId() == $quoteItemToParse->getProductId()) {
                        $quoteItem2->setSimpleBundleDiscountTaxAmount($quoteItem2->getSimpleBundleDiscountTaxAmount() + $itemDiscountTaxAmount);
                        $quoteItem2->setBaseSimpleBundleDiscountTaxAmount($quoteItem2->getBaseSimpleBundleDiscountTaxAmount() + $baseItemDiscountTaxAmount);
                    }
                }
                
                if ($quoteItem->getProductType() == 'bundle' && $quoteItemToParse->getParentItemId() == $quoteItem->getId()) {
                    $quoteItem->setSimpleBundleDiscountTaxAmount($quoteItem->getSimpleBundleDiscountTaxAmount() + $itemDiscountTaxAmount);
                }
               
                $bundleDiscountTaxAmount += $itemDiscountTaxAmount;
                $bundleBaseDiscountTaxAmount += $baseItemDiscountTaxAmount;
            }

        }
        
        $this->_baseSimpleBundleAmount += $quote->getStore()->roundPrice($bundleBaseDiscountAmount);
        $this->_simpleBundleAmount += $quote->getStore()->roundPrice($bundleDiscountAmount);
        $this->_baseSimpleBundleTaxAmount += $quote->getStore()->roundPrice($bundleBaseDiscountTaxAmount);
        $this->_simpleBundleTaxAmount += $quote->getStore()->roundPrice($bundleDiscountTaxAmount);
        return true;
    }
    
    protected function _getQuoteItemPrice($quoteItem, $base = false) {
       if($base) {
           return $quoteItem->getPriceInclTax() ? $quoteItem->getPriceInclTax() : $quoteItem->getPrice() + $quoteItem->getTaxAmount();
       }
       return $quoteItem->getBasePriceInclTax() ? $quoteItem->getBasePriceInclTax() : $quoteItem->getBasePrice() + $quoteItem->getBaseTaxAmount();
    }

}

/*
 * $itemsToManage = array();
                        if($quoteItem->getHasChildren()) {
                            foreach($quoteItem->getChildren() as $child) $itemsToManage[] = $child;
                        }
                        $itemsToManage[] = $quoteItem;
 */