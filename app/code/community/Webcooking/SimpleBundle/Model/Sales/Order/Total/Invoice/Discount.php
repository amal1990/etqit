<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Model_Sales_Order_Total_Invoice_Discount extends Mage_Sales_Model_Order_Invoice_Total_Abstract {

   
    
    public function collect(Mage_Sales_Model_Order_Invoice $invoice) {
        $order = $invoice->getOrder();
        if($order->getSbundleDiscountAmount()>0 && $order->getBaseSbundleDiscountAmount()>0) {
            $baseSimpleBundleDiscountAmount = $simpleBundleDiscountAmount = 0;
            $baseSimpleBundleDiscountTaxAmount = $simpleBundleDiscountTaxAmount = 0;
            foreach($invoice->getAllItems() as $item) {
                $orderItem = $item->getOrderItem();
                if ($orderItem->isDummy()) {
                    continue;
                }
                $orderItemDiscount      = (float) $orderItem->getSimpleBundleDiscountAmount();
                $baseOrderItemDiscount  = (float) $orderItem->getBaseSimpleBundleDiscountAmount();
                $orderItemDiscountTax      = (float) $orderItem->getSimpleBundleDiscountTaxAmount();
                $baseOrderItemDiscountTax  = (float) $orderItem->getBaseSimpleBundleDiscountTaxAmount();

                $orderItemQty       = $orderItem->getQtyOrdered();
                $discount = $orderItemDiscount - $orderItem->getSimpleBundleDiscountInvoiced();
                $baseDiscount = $baseOrderItemDiscount - $orderItem->getBaseSimpleBundleDiscountInvoiced();
                $discountTax = $orderItemDiscountTax - $orderItem->getSimpleBundleDiscountTaxInvoiced();
                $baseDiscountTax = $baseOrderItemDiscountTax - $orderItem->getBaseSimpleBundleDiscountTaxInvoiced();
                if (!$item->isLast()) {
                    $activeQty = $orderItemQty - $orderItem->getQtyInvoiced();
                    $discount = $invoice->roundPrice($discount / $activeQty * $item->getQty(), 'regular', true);
                    $baseDiscount = $invoice->roundPrice($baseDiscount / $activeQty * $item->getQty(), 'base', true);
                    $discountTax = $invoice->roundPrice($discountTax / $activeQty * $item->getQty(), 'regular', true);
                    $baseDiscountTax = $invoice->roundPrice($baseDiscountTax / $activeQty * $item->getQty(), 'base', true);
                }

                $item->setSimpleBundleDiscountAmount($discount);
                $item->setBaseSimpleBundleDiscountAmount($baseDiscount);
                $item->setSimpleBundleDiscountTaxAmount($discountTax);
                $item->setBaseSimpleBundleDiscountTaxAmount($baseDiscountTax);

                $simpleBundleDiscountAmount += $discount;
                $baseSimpleBundleDiscountAmount += $baseDiscount;
                $simpleBundleDiscountTaxAmount += $discountTax;
                $baseSimpleBundleDiscountTaxAmount += $baseDiscountTax;
                
            }
            
            
            
            $invoice->setBaseSbundleDiscountAmount($baseSimpleBundleDiscountAmount);
            $invoice->setSbundleDiscountAmount($simpleBundleDiscountAmount);
            
            //echo $invoice->getDiscountAmount().'+'.$simpleBundleDiscountAmount.'-'.$simpleBundleDiscountTaxAmount;
            if(version_compare(Mage::getVersion(),'1.7.0.2') > 0) {
                $invoice->setDiscountAmount($invoice->getDiscountAmount()+$simpleBundleDiscountAmount-$simpleBundleDiscountTaxAmount);
                $invoice->setBaseDiscountAmount($invoice->getBaseDiscountAmount()+$baseSimpleBundleDiscountAmount-$baseSimpleBundleDiscountTaxAmount);
            } else {
                $invoice->setDiscountAmount($invoice->getDiscountAmount()-$simpleBundleDiscountAmount+$baseSimpleBundleDiscountTaxAmount);
                $invoice->setBaseDiscountAmount($invoice->getBaseDiscountAmount()-$baseSimpleBundleDiscountAmount+$simpleBundleDiscountTaxAmount);
            }
            
        }
        return $this;
    }

}