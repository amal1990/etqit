<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Model_Sales_Order_Total_Creditmemo_Discount extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract {

   
    
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo) {
        $order = $creditmemo->getOrder();
        if($order->getSbundleDiscountAmount()>0 && $order->getBaseSbundleDiscountAmount()>0) {
            $baseSimpleBundleDiscountAmount = $simpleBundleDiscountAmount = 0;
            $baseSimpleBundleDiscountTaxAmount = $simpleBundleDiscountTaxAmount = 0;
            foreach($creditmemo->getAllItems() as $item) {
                $orderItem = $item->getOrderItem();
                if ($orderItem->isDummy()) {
                    continue;
                }
                $orderItemDiscount      = (float) $orderItem->getSimpleBundleDiscountAmount();
                $baseOrderItemDiscount  = (float) $orderItem->getBaseSimpleBundleDiscountAmount();
                $orderItemDiscountTax      = (float) $orderItem->getSimpleBundleDiscountTaxAmount();
                $baseOrderItemDiscountTax  = (float) $orderItem->getBaseSimpleBundleDiscountTaxAmount();

                $orderItemQty       = $orderItem->getQtyOrdered();
                $discount = $orderItemDiscount - $orderItem->getSimpleBundleDiscountRefunded();
                $baseDiscount = $baseOrderItemDiscount - $orderItem->getBaseSimpleBundleDiscountRefunded();
                $discountTax = $orderItemDiscountTax - $orderItem->getSimpleBundleDiscountTaxRefunded();
                $baseDiscountTax = $baseOrderItemDiscountTax - $orderItem->getBaseSimpleBundleDiscountTaxRefunded();
                if (!$item->isLast()) {
                    $activeQty = $orderItemQty - $orderItem->getQtyRefunded();
                    $discount = $creditmemo->roundPrice($discount / $activeQty * $item->getQty(), 'regular', true);
                    $baseDiscount = $creditmemo->roundPrice($baseDiscount / $activeQty * $item->getQty(), 'base', true);
                    $discountTax = $creditmemo->roundPrice($discountTax / $activeQty * $item->getQty(), 'regular', true);
                    $baseDiscountTax = $creditmemo->roundPrice($baseDiscountTax / $activeQty * $item->getQty(), 'base', true);
                }

                $item->setSimpleBundleDiscountAmount($discount);
                $item->setBaseSimpleBundleDiscountAmount($baseDiscount);
                $item->setSimpleBundleDiscountTaxAmount($discountTax);
                $item->setBaseSimpleBundleDiscountTaxAmount($baseDiscountTax);


                $simpleBundleDiscountAmount += $discount;
                $baseSimpleBundleDiscountAmount += $baseDiscount;
                $simpleBundleDiscountTaxAmount += $discountTax;
                $baseSimpleBundleDiscountTaxAmount += $baseDiscountTax;
            }
           
            
            $creditmemo->setBaseSbundleDiscountAmount($baseSimpleBundleDiscountAmount);
            $creditmemo->setSbundleDiscountAmount($simpleBundleDiscountAmount);
            
            //echo $creditmemo->getDiscountAmount().'-'.$simpleBundleDiscountAmount.'+'.$simpleBundleDiscountTaxAmount;
            if(version_compare(Mage::getVersion(),'1.7.0.2') > 0) {
                $creditmemo->setDiscountAmount($creditmemo->getDiscountAmount()+$simpleBundleDiscountAmount-$simpleBundleDiscountTaxAmount);
                $creditmemo->setBaseDiscountAmount($creditmemo->getBaseDiscountAmount()+$baseSimpleBundleDiscountAmount-$baseSimpleBundleDiscountTaxAmount);
            } else {
                $creditmemo->setDiscountAmount($creditmemo->getDiscountAmount()-$simpleBundleDiscountAmount+$simpleBundleDiscountTaxAmount);
                $creditmemo->setBaseDiscountAmount($creditmemo->getBaseDiscountAmount()-$baseSimpleBundleDiscountAmount+$baseSimpleBundleDiscountTaxAmount);
            }
            
        }
        return $this;
    }

}