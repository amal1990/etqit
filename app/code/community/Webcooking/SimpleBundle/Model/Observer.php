<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Model_Observer {

    public function observeCheckoutCartSaveAfter($observer) {
        $cart = $observer->getEvent()->getCart();
        if($cart->getItemsCount() <= 0) {
            Mage::getSingleton('checkout/session')->setSimpleBundleIds(array());
        }
    }
    
    public function observeSalesruleValidatorProcess($observer) {
        $rule = $observer->getEvent()->getRule();
        $item = $observer->getEvent()->getItem();
        $address = $observer->getEvent()->getAddress();
        $quote = $observer->getEvent()->getQuote();
        $qty = $observer->getEvent()->getQty();
        $result = $observer->getEvent()->getResult();
        $this->_manageExcludeSimpleBundleInShoppingcartRules($rule, $item, $address, $quote, $qty, $result);
        $this->_manageDiscountOnBundlePrice($rule, $item, $address, $quote, $qty, $result);
    }

    public function observeAdminhtmlBlockSalesruleActionsPrepareform($observer) {
        $form = $observer->getEvent()->getForm();
        $this->_addExcludeSimpleBundleShoppingcartToRule($form);
    }

    public function observeCatalogProductPrepareSave($observer) {
        $product = $observer->getEvent()->getProduct();
        $request = $observer->getEvent()->getRequest();

        $this->_saveBundles($product, $request);
    }

    public function observePaypalPrepareLineItems($observer) {
        $paypalCart = $observer->getEvent()->getPaypalCart();
        $this->_updatePaypalTotal($paypalCart);
    }

    public function observeSalesOrderInvoiceSaveAfter($observer) {
        $invoice = $observer->getEvent()->getInvoice();
        $this->_invoiceSaveAfter($invoice);
    }

    public function observeSalesOrderCreditmemoSaveAfter($observer) {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $this->_creditmemoSaveAfter($creditmemo);
    }

    public function observeSalesInvoiceItemSaveAfter($observer) {
        $invoiceItem = $observer->getEvent()->getInvoiceItem();
        $this->_invoiceItemSaveAfter($invoiceItem);
    }

    public function observeSalesCreditmemoItemSaveAfter($observer) {
        $creditmemoItem = $observer->getEvent()->getCreditmemoItem();
        $this->_creditmemoItemSaveAfter($creditmemoItem);
    }
    
     protected function _invoiceItemSaveAfter($invoiceItem) {
        if ($invoiceItem->getBaseSimpleBundleDiscountAmount()) {
            $orderItem = $invoiceItem->getOrderItem();
            $orderItem->setSimpleBundleDiscountInvoiced($orderItem->getSimpleBundleDiscountInvoiced() + $invoiceItem->getSimpleBundleDiscountAmount());
            $orderItem->setBaseSimpleBundleDiscountInvoiced($orderItem->getBaseSimpleBundleDiscountInvoiced() + $invoiceItem->getBaseSimpleBundleDiscountAmount());
            $orderItem->setSimpleBundleDiscountTaxInvoiced($orderItem->getSimpleBundleDiscountTaxInvoiced() + $invoiceItem->getSimpleBundleDiscountTaxAmount());
            $orderItem->setBaseSimpleBundleDiscountTaxInvoiced($orderItem->getBaseSimpleBundleDiscountTaxInvoiced() + $invoiceItem->getBaseSimpleBundleDiscountTaxAmount());
            $orderItem->save();
        }
        return $this;
    }
    
    
     protected function _creditmemoItemSaveAfter($creditmemoItem) {
        if ($creditmemoItem->getBaseSimpleBundleDiscountAmount()) {
            $orderItem = $creditmemoItem->getOrderItem();
            $orderItem->setSimpleBundleDiscountRefunded($orderItem->getSimpleBundleDiscountRefunded() + $creditmemoItem->getSimpleBundleDiscountAmount());
            $orderItem->setBaseSimpleBundleDiscountRefunded($orderItem->getBaseSimpleBundleDiscountRefunded() + $creditmemoItem->getBaseSimpleBundleDiscountAmount());
            $orderItem->setSimpleBundleDiscountTaxRefunded($orderItem->getSimpleBundleDiscountTaxRefunded() + $creditmemoItem->getSimpleBundleDiscountTaxAmount());
            $orderItem->setBaseSimpleBundleDiscountTaxRefunded($orderItem->getBaseSimpleBundleDiscountTaxRefunded() + $creditmemoItem->getBaseSimpleBundleDiscountTaxAmount());
            $orderItem->save();
        }
        return $this;
    }

    protected function _invoiceSaveAfter($invoice) {
        if ($invoice->getBaseSbundleDiscountAmount()) {
            $order = $invoice->getOrder();
            $order->setSbundleDiscountAmountInvoiced($order->getSbundleDiscountAmountInvoiced() + $invoice->getSbundleDiscountAmount());
            $order->setBaseSbundleDiscountAmountInvoiced($order->getBaseSbundleDiscountAmountInvoiced() + $invoice->getBaseSbundleDiscountAmount());
        }
        return $this;
    }

    protected function _manageDiscountOnBundlePrice($rule, $item, $address, $quote, $qty, $result) {
        if (!Mage::getStoreConfigFlag('simplebundle/general/apply_sales_rules_on_bundle_price')) {
            return;
        }
        if(!in_array($rule->getSimpleAction(), array(Mage_SalesRule_Model_Rule::TO_PERCENT_ACTION, Mage_SalesRule_Model_Rule::BY_PERCENT_ACTION))) {
            return;
        }
        if ($item->getSimpleBundlePercentDiscount() && $item->getUsedInSimpleBundle() <= $qty) {
            $percent = $item->getSimpleBundlePercentDiscount();
            $discountAmount = $result->getDiscountAmount();
            $baseDiscountAmount = $result->getBaseDiscountAmount();
            
            $unitDiscountAmount = $discountAmount / $qty;
            $unitBaseDiscountAmount = $baseDiscountAmount / $qty;

            $discountAmount = $unitDiscountAmount * ($qty - $item->getUsedInSimpleBundle()); // normal discount
            $discountAmount += (($unitDiscountAmount - $unitDiscountAmount * $percent) * $item->getUsedInSimpleBundle()); //discount on simple bundle discount
            $baseDiscountAmount = $unitBaseDiscountAmount * ($qty - $item->getUsedInSimpleBundle()); // normal discount
            $baseDiscountAmount += (($unitBaseDiscountAmount - $unitBaseDiscountAmount * $percent) * $item->getUsedInSimpleBundle()); //discount on simple bundle discount

            $result->setDiscountAmount($discountAmount);
            $result->setBaseDiscountAmount($baseDiscountAmount);
        }
    }

    protected function _manageExcludeSimpleBundleInShoppingcartRules($rule, $item, $address, $quote, $qty, $result) {
        if (!$quote->getSimpleBundleProcessed()) {
            Mage::getModel('simplebundle/sales_quote_address_total_sbundle')->process($quote);
        }
        if ($rule->getExcludeSimpleBundle() && $item->getUsedInSimpleBundle()) {
            $qtyUsedInBundles = $item->getUsedInSimpleBundle();
            $result->discount_amount = ($result->discount_amount / $qty) * ($qty - $qtyUsedInBundles);
            $result->base_discount_amount = ($result->base_discount_amount / $qty) * ($qty - $qtyUsedInBundles);
        }
    }

    protected function _addExcludeSimpleBundleShoppingcartToRule($form) {
        $form->getElement('action_fieldset')->addField('exclude_simple_bundle', 'select', array(
            'label' => Mage::helper('simplebundle')->__('Exclude simple bundle items ?'),
            'title' => Mage::helper('simplebundle')->__('Exclude simple bundle items ?'),
            'name' => 'exclude_simple_bundle',
            'options' => array(
                '1' => Mage::helper('salesrule')->__('Yes'),
                '0' => Mage::helper('salesrule')->__('No'),
            ),
        ));
        //$model = Mage::registry('current_promo_quote_rule');
        //$form->getElement('coupon_exclusive')->setValue($model->getCouponExclusive());
    }

    protected function _creditmemoSaveAfter($creditmemo) {
        if ($creditmemo->getSbundleDiscountAmount()) {
            $order = $creditmemo->getOrder();
            $order->setSbundleDiscountAmountRefunded($order->getSbundleDiscountAmountRefunded() + $creditmemo->getSbundleDiscountAmount());
            $order->setBaseSbundleDiscountAmountRefunded($order->getBaseSbundleDiscountAmountRefunded() + $creditmemo->getBaseSbundleDiscountAmount());
        }
        return $this;
    }

    protected function _updatePaypalTotal($paypalCart) {
        if ($paypalCart && $paypalCart->getSalesEntity()) {
            $amount = $paypalCart->getSalesEntity()->getSbundleDiscountAmount();
            $taxAmount = $paypalCart->getSalesEntity()->getSbundleDiscountTax();
            //Mage::log($paypalCart->getSalesEntity()->getData(),null, 'payment_paypal_standard.log');
            if ($amount > 0) {
                //Mage::log(($amount-$taxAmount),null, 'payment_paypal_standard.log');
                //$paypalCart->addItem(Mage::helper('simplebundle')->__('Bundles discount amount'), 1, -1.00 * ($amount-$taxAmount), NULL);  //Paypal standard does not accept negative amount for items
                $paypalCart->updateTotal(Mage_Paypal_Model_Cart::TOTAL_DISCOUNT, ($amount - $taxAmount), Mage::helper('simplebundle')->__('Bundles discount amount'));
            }
        }
    }

    protected function _saveBundles($product, $request) {
        $simplebundleData = $request->getParam('simplebundle');
        $simplebundleSelectionData = $request->getParam('simplebundle_selections');
        if (!$simplebundleData)
            $simplebundleData = array();
        foreach ($simplebundleData as $idx => $data) {
            $selection = isset($simplebundleSelectionData[$idx]) ? $simplebundleSelectionData[$idx] : false;
            //$storeId = $request->getParam('store');
            $bundle = Mage::getModel('simplebundle/bundle');
            if (isset($data['simple_bundle_id'])) {
                $bundle->load($data['simple_bundle_id']);
            }
            if (isset($data['simple_bundle_id']) && $data['delete'] == 1) {
                $bundle->delete();
            } else if ($data['delete'] != 1) {
                $bundle->setProductId($product->getId());
                $bundle->setTemplateId($data['template_id']?$data['template_id']:null);
                if(!$bundle->getTemplateId()) {
                    $bundle->setBundleName($data['bundle_name']);
                    $bundle->setActive($data['active']);
                    $bundle->setDiscountAmount($data['discount_amount']);
                    $bundle->setDiscountType($data['discount_type']);
                    $bundle->setStores($data['stores'] ? $data['stores'] : '0');
                    $bundle->setBaseQty($data['base_qty']);
                    $bundle->setExcludeBaseProductFromDiscount($data['exclude_base_product_from_discount']);
                    $bundle->setSpecialPriceBehavior($data['special_price_behavior']);
                }
                $bundle->setPosition($data['position']);
                if (!isset($data['simple_bundle_id'])) {
                    $bundle->setCreatedAt(date('Y-m-d H:i:s'));
                }
                $bundle->setUpdatedAt(date('Y-m-d H:i:s'));
                $bundle->save();

                if ($selection) {
                    foreach ($selection as $item) {
                        $bundleItem = Mage::getModel('simplebundle/bundle_item');
                        if (isset($item['simple_bundle_item_id'])) {
                            $bundleItem->load($item['simple_bundle_item_id']);
                        }
                        if (isset($item['simple_bundle_item_id']) && $item['delete'] == 1) {
                            $bundleItem->delete();
                        } else if ($item['delete'] != 1) {
                            $bundleItem->simple_bundle_id = $bundle->getId();
                            $bundleItem->product_id = $item['product_id'];
                            $bundleItem->selection_qty = $item['selection_qty'];
                            $bundleItem->position = $item['position'];
                            $bundleItem->save();
                        }
                    }
                }
            }
        }
    }

}
