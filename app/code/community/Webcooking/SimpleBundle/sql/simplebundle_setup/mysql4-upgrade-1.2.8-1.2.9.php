<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
$installer = $this;
$installer->startSetup();


$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('wcoo_simple_bundle_template')} (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `stores` varchar(255) NOT NULL DEFAULT '0',
  `position` int(11) DEFAULT '0',
  `discount_amount` decimal(12,2) DEFAULT NULL,
  `discount_type` varchar(127) NOT NULL DEFAULT '0',
  `base_qty` int(10) unsigned DEFAULT '1',
  `exclude_base_product_from_discount` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS {$this->getTable('wcoo_simple_bundle_template_item')} (
  `template_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `selection_qty` int(10) NOT NULL DEFAULT '1',
  `position` int(11) DEFAULT '0',
  PRIMARY KEY (`template_item_id`),
  KEY `template_id` (`template_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


ALTER TABLE {$this->getTable('wcoo_simple_bundle_template_item')}
  ADD CONSTRAINT `simple_bundle_template_item_template_id_fk` FOREIGN KEY (`template_id`) 
  REFERENCES {$this->getTable('wcoo_simple_bundle_template')} (`template_id`) 
      ON DELETE CASCADE 
      ON UPDATE CASCADE,
  ADD CONSTRAINT `simple_bundle_template_item_product_id_fk` FOREIGN KEY (`product_id`) 
  REFERENCES {$this->getTable('catalog_product_entity')} (`entity_id`) 
  ON DELETE CASCADE 
  ON UPDATE CASCADE;




ALTER TABLE {$this->getTable('wcoo_simple_bundle')} 
      ADD COLUMN `template_id` tinyint(1) NULL;


ALTER TABLE {$this->getTable('wcoo_simple_bundle')}
  ADD CONSTRAINT `wcoo_simple_bundle_template_id_fk` 
  FOREIGN KEY (`template_id`) 
  REFERENCES {$this->getTable('wcoo_simple_bundle_template')} (`template_id`) 
      ON DELETE CASCADE 
      ON UPDATE CASCADE;
      

    ");

$installer->endSetup();