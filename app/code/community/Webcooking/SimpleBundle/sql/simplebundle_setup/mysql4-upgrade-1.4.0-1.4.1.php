<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('wcoo_simple_bundle')} 
      ADD COLUMN `special_price_behavior` tinyint(3) UNSIGNED NOT NULL default '0' AFTER `exclude_base_product_from_discount`;
ALTER TABLE {$this->getTable('wcoo_simple_bundle_template')} 
      ADD COLUMN `special_price_behavior` tinyint(3) UNSIGNED NOT NULL default '0' AFTER `exclude_base_product_from_discount`;
ALTER TABLE {$this->getTable('wcoo_simple_bundle_template')} 
      ADD COLUMN `conditions_serialized` TEXT NULL AFTER `special_price_behavior`;
ALTER TABLE {$this->getTable('wcoo_simple_bundle_template')} 
      ADD COLUMN `rule_status` tinyint(1) NOT NULL DEFAULT '1' AFTER `conditions_serialized`;
      
      
UPDATE {$this->getTable('wcoo_simple_bundle')} set special_price_behavior = '3';
UPDATE {$this->getTable('wcoo_simple_bundle_template')} set special_price_behavior = '3';
");

$installer->endSetup();