<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */

class Webcooking_SimpleBundle_Block_Adminhtml_List_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('simplebundleGrid');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
    }

    protected function _prepareCollection()
    {
        $sbundleCollection = Mage::getModel('simplebundle/bundle')->getCollection()
                ->skipAllChecks()
                ->joinMasterProduct();
        //echo $sbundleCollection->getSelect();die();
        $this->setCollection($sbundleCollection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $baseUrl = $this->getUrl();


        $this->addColumn('simple_bundle_id', array(
            'header'    => Mage::helper('simplebundle')->__('ID'),
            'align'     => 'left',
            'index'     => 'simple_bundle_id',
            'type'		=> 'number'
        ));
        
        
        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('stores', array(
                'header'        => Mage::helper('simplebundle')->__('Store View'),
                'index'         => 'stores',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => true,
                'filter_condition_callback'
                                => array($this, '_filterStoreCondition'),
            ));
        }
        
        $this->addColumn('active', array(
            'header'    => Mage::helper('simplebundle')->__('Status'),
            'index'     => 'active',
            'type'      => 'options',
            'options'   => array(
                0 => Mage::helper('simplebundle')->__('Disabled'),
                1 => Mage::helper('simplebundle')->__('Enabled')
            ),
        ));
        
        
        $this->addColumn('template_id', array(
            'header'    => Mage::helper('simplebundle')->__('Template'),
            'index'     => 'template_id',
            'width'     => '20',
            'type'      => 'options',
            'options'   => Mage::helper('simplebundle')->getTemplateOptions()
        ));
        
        
        $this->addColumn('master_product', array(
            'header'    => Mage::helper('simplebundle')->__('Master product'),
            'index'     => 'product_id',
            'filter_condition_callback' => array($this, 'filterMasterProduct'),
            'frame_callback' => array($this, 'decorateMasterProduct'),
        ));
        
        $this->addColumn('slave_products', array(
            'header'    => Mage::helper('simplebundle')->__('Slave products'),
            'index'     => 'product_id',
            'filter' => false,
            'frame_callback' => array($this, 'decorateSlaveProduct'),
        ));
        
        
        $this->addColumn('discount_amount', array(
            'header'    => Mage::helper('simplebundle')->__('Discount Amount'),
            'index'     => 'discount_amount',
            'type'      => 'number',
        ));
        
        $this->addColumn('discount_type', array(
            'header'    => Mage::helper('simplebundle')->__('Discount Type'),
            'index'     => 'discount_type',
            'type'      => 'options',
            'options'   => array(
                'percent' => Mage::helper('simplebundle')->__('Percentage'),
                'fixed' => Mage::helper('simplebundle')->__('Fixed amount')
            ),
        ));
        
        
        $this->addColumn('created_at', array(
            'header'    => Mage::helper('simplebundle')->__('Date Created'),
            'type'     => 'datetime',
            'index'     => 'created_at',
        ));

        return parent::_prepareColumns();
    }

    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
        foreach($this->getCollection() as $item) {
            $item->setStores(explode(',', $item->getStores()));
        }
    }

    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }
        $this->getCollection()->addStoreFilter($value);
    }

    /**
     * Row click url
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('adminhtml/catalog_product/edit', array('id'=>$row['product_id'], 'tab'=>'product_info_tabs_simple_bundle'));
    }
    
    
    
    public function decorateMasterProduct($value, $row, $column, $isExport) {
        $value  = $row['sku'];
        $value .= '<br/>';
        $value .= $row['name'];
        $value .= '<br/>';
        $value .= $this->__('Qty : %s', $row['base_qty']);
        return $value;
    }

    public function filterMasterProduct($collection, $column) {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $collection->getSelect()->where(
                "sku like ?
            OR name_table.value like ?
            OR product_id like ?"
                , "%$value%");

        return $this;
    }
    
    public function decorateSlaveProduct($value, $row, $column, $isExport) {
        $values = array();
        $row->loadSelections(true);
        foreach($row->getSelections() as $selection) {
            $value  = $selection->getSku();
            $value .= '<br/>';
            $value .= $selection->getName();
            $value .= '<br/>';
            $value .= $this->__('Qty : %s', $selection->getSelectionQty());
            $values[] = $value;
        }
        $value = implode('<br/><br/>', $values);
        return $value;
    }

    
    
    protected function _exportCsvItem(Varien_Object $item, Varien_Io_File $adapter) {
        $row = array();
        $row[] = $item->getId();
        $row[] = $item->getBundleName();
        $row[] = $item->getStores();
        $row[] = $item->getActive();
        $row[] = $item->getSku();
        $row[] = $item->getBaseQty();
        $row[] = $item->getDiscountAmount();
        $row[] = $item->getDiscountType();
        $row[] = $item->getExcludeBaseProductFromDiscount();
        $row[] = $item->getSpecialPriceBehavior();
        $row[] = $item->getPosition();
        $cpt = 0;
        $item->loadSelections(true);
        foreach($item->getSelections() as $selection) {
            $row[] = $selection->getSimpleBundleItemId();
            $row[] = $selection->getSku();
            $row[] = $selection->getSelectionQty();
            $row[] = $selection->getPosition();
            $cpt++;
        }
        for($i=$cpt; $i<50; $i++) {
            $row[] = '';
            $row[] = '';
            $row[] = '';
            $row[] = '';
        }
        $adapter->streamWriteCsv($row);
    }

    protected function _getExportHeaders() {
        $row = array();
        $row[] = 'simple_bundle_id';
        $row[] = 'bundle_name';
        $row[] = 'stores';
        $row[] = 'active';
        $row[] = 'sku';
        $row[] = 'base_qty';
        $row[] = 'discount_amount';
        $row[] = 'discount_type';
        $row[] = 'exclude_base_product_from_discount';
        $row[] = 'special_price_behavior';
        $row[] = 'position';
        for($i=0; $i<50; $i++) {
            $row[] = 'simple_bundle_item_id_'.($i+1);
            $row[] = 'simple_bundle_item_sku_'.($i+1);
            $row[] = 'simple_bundle_item_qty_'.($i+1);
            $row[] = 'simple_bundle_item_position_'.($i+1);
        }
        return $row;
    }
    
    
    
    
}
