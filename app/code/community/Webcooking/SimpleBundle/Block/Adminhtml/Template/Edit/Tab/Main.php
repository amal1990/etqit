<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */

class Webcooking_SimpleBundle_Block_Adminhtml_Template_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    protected function _prepareForm()
    {/** @var Cms_Model_Page */
        $model = Mage::registry('simplebundle_template');

        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('template_');

        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('simplebundle')->__('Template information'),'class'=>'fieldset-wide'));

        if ($model->getTemplateId()) {
        	$fieldset->addField('template_id', 'hidden', array(
                'name' => 'template_id',
            ));
        }
        

    	$fieldset->addField('name', 'text', array(
            'name'      => 'template_name',
            'label'     => Mage::helper('simplebundle')->__('Name'),
            'title'     => Mage::helper('simplebundle')->__('Name'),
            'required'  => true,
        ));
        

    	$fieldset->addField('bundle_name', 'text', array(
            'name'      => 'bundle_name',
            'label'     => Mage::helper('simplebundle')->__('Bundle Name'),
            'title'     => Mage::helper('simplebundle')->__('Bundle Name'),
            'required'  => false,
            'comment'   => Mage::helper('simplebundle')->__('Leave empty to display products names.'),
        ));
        
    	$fieldset->addField('active', 'select', array(
            'label'     => Mage::helper('simplebundle')->__('Status'),
            'title'     => Mage::helper('simplebundle')->__('Status'),
            'name'      => 'active',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('simplebundle')->__('Enabled'),
                '0' => Mage::helper('simplebundle')->__('Disabled'),
            ),
        ));
        
        
        $fieldset->addField('all_stores', 'select', array(
            'name'      => 'all_stores',
            'label'     => Mage::helper('simplebundle')->__('All Stores'),
            'title'     => Mage::helper('simplebundle')->__('All Stores'),
            'required'  => false,
            'options'   => array(
                '1' => Mage::helper('simplebundle')->__('Yes'),
                '0' => Mage::helper('simplebundle')->__('No'),
                )
        ));
        
        /**
         * Check is single store mode
         */
        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('stores', 'multiselect', array(
                'name'      => 'stores',
                'label'     => Mage::helper('simplebundle')->__('Store View'),
                'title'     => Mage::helper('simplebundle')->__('Store View'),
                'required'  => false,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, false),
            ));
        }
        else {
            $storeId = Mage::app()->getDefaultStoreView()->getId();
            $fieldset->addField('stores', 'hidden', array(
                'name'      => 'stores',
                'value'     => $storeId
            ));
            $model->setStoreId($storeId);
        }
        
        
        

    	$fieldset->addField('position', 'text', array(
            'name'      => 'position',
            'label'     => Mage::helper('simplebundle')->__('Position'),
            'title'     => Mage::helper('simplebundle')->__('Position'),
            'required'  => true,
        ));
        
    	$fieldset->addField('discount_amount', 'text', array(
            'name'      => 'discount_amount',
            'label'     => Mage::helper('simplebundle')->__('Discount Amount'),
            'title'     => Mage::helper('simplebundle')->__('Discount Amount'),
            'required'  => true,
        ));
        
    	$fieldset->addField('discount_type', 'select', array(
            'name'      => 'discount_type',
            'label'     => Mage::helper('simplebundle')->__('Discount Type'),
            'title'     => Mage::helper('simplebundle')->__('Discount Type'),
            'required'  => true,
            'options'   => array(
                'percent' => Mage::helper('simplebundle')->__('Percentage'),
                'fixed' => Mage::helper('simplebundle')->__('Fixed')
            )
        ));
        
        
    	$fieldset->addField('base_qty', 'text', array(
            'name'      => 'base_qty',
            'label'     => Mage::helper('simplebundle')->__('Main Product Qty'),
            'title'     => Mage::helper('simplebundle')->__('Main Product Qty'),
            'required'  => true,
        ));
        
        
        
        
        $fieldset->addField('exclude_base_product_from_discount', 'select', array(
            'name'      => 'exclude_base_product_from_discount',
            'label'     => Mage::helper('simplebundle')->__('Exclude Base Product From Discount'),
            'title'     => Mage::helper('simplebundle')->__('Exclude Base Product From Discount'),
            'required'  => false,
            'options'   => array(
                '1' => Mage::helper('simplebundle')->__('Yes'),
                '0' => Mage::helper('simplebundle')->__('No'),
                )
        ));
        
        $fieldset->addField('special_price_behavior', 'select', array(
            'name'      => 'special_price_behavior',
            'label'     => Mage::helper('simplebundle')->__('Special Price Behavior'),
            'title'     => Mage::helper('simplebundle')->__('Special Price Behavior'),
            'required'  => false,
            'options'   => Mage::helper('simplebundle')->getSpecialPriceBehaviorOptions()
        ));
        
        $data = $model->getData();
        if(!isset($data['base_qty'])) {
            $data['base_qty'] = 1;
        }
        if(isset($data['stores']) && $data['stores'] == 0) {
            $data['all_stores'] = 1;
        }
        
        $form->setValues($data);
        $this->setForm($form);

        return parent::_prepareForm();
    }
    
    public function getTabLabel() {
        return $this->__('General Information');
    }
    
    public function getTabTitle() {
        return $this->getTabLabel();
    }

    public function canShowTab() {
        return true;
    }
    
    public function isHidden() {
        return false;
    }
}
