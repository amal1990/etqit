<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Block_Adminhtml_Template_Edit_Tab_Rule extends Mage_Adminhtml_Block_Widget_Form implements Mage_Adminhtml_Block_Widget_Tab_Interface {

    protected function _prepareForm()
    {
        $templateModel = Mage::registry('simplebundle_template');
        $model = $templateModel->getRule();

        $form = new Varien_Data_Form();

        $form->setHtmlIdPrefix('rule_');
        
        
        $fieldset = $form->addFieldset('general_fieldset', array(
            'legend'=>Mage::helper('simplebundle')->__('Options')
        ));
        
        $fieldset->addField('rule_status', 'select', array(
            'label'     => Mage::helper('simplebundle')->__('Rule status'),
            'title'     => Mage::helper('simplebundle')->__('Rule status'),
            'name'      => 'rule_status',
            'required'  => true,
            'options'   => array(
                '1' => Mage::helper('simplebundle')->__('Enabled'),
                '0' => Mage::helper('simplebundle')->__('Disabled'),
            ),
        ));
        

        $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
            ->setTemplate('webcooking/simplebundle/template/edit/tab/rule/fieldset.phtml')
            ->setNewChildUrl($this->getUrl('adminhtml/simplebundle_template_rule/newConditionHtml/form/rule_conditions_fieldset'));

        $ruleResultsBlock = $this->getLayout()->createBlock('simplebundle/adminhtml_template_edit_tab_rule_result', 'results');
        $renderer->append($ruleResultsBlock);
       
        $fieldset = $form->addFieldset('conditions_fieldset', array(
            'legend'=>Mage::helper('simplebundle')->__('Apply the template when the following conditions are met (leave blank for all products)')
        ))->setRenderer($renderer);

        $fieldset->addField('conditions', 'text', array(
            'name' => 'conditions',
            'label' => Mage::helper('simplebundle')->__('Conditions'),
            'title' => Mage::helper('simplebundle')->__('Conditions'),
        ))->setRule($model)->setRenderer(Mage::getBlockSingleton('rule/conditions'));

        $values = $model->getData();
        $values['rule_status'] = $templateModel->getRuleStatus();
        $form->setValues($values);


        $this->setForm($form);

        return parent::_prepareForm();
    }
    
    public function getTabLabel() {
        return $this->__('Template association rules');
    }
    
    public function getTabTitle() {
        return $this->getTabLabel();
    }

    public function canShowTab() {
        return true;
    }
    
    public function isHidden() {
        return false;
    }
    

}
