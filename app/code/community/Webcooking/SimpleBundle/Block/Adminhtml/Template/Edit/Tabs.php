<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */

class Webcooking_SimpleBundle_Block_Adminhtml_Template_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('sb_template_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('simplebundle')->__('Template information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('main_section', array(
            'label'     => Mage::helper('simplebundle')->__('General Information'),
            'title'     => Mage::helper('simplebundle')->__('General Information'),
            'content'   => $this->getLayout()->createBlock('simplebundle/adminhtml_template_edit_tab_main')->toHtml(),
            'active'    => true
        ));
        
        $model = Mage::registry('simplebundle_template');
        if($model->getId()) {
            $this->addTab('sub_products', array(
                'label'     => Mage::helper('simplebundle')->__('Bundle products'),
                'title'     => Mage::helper('simplebundle')->__('Bundle products'),
                'content'   => $this->getLayout()->createBlock('simplebundle/adminhtml_template_edit_tab_bundle')->toHtml(),
                'active'    => false
            ));

            $this->addTab('products', array(
                'label'     => Mage::helper('simplebundle')->__('Template association'),
                'title'     => Mage::helper('simplebundle')->__('Template association'),
                'content'   => $this->getLayout()->createBlock('simplebundle/adminhtml_template_edit_tab_association')->toHtml(),
                'active'    => false
            ));

            $this->addTab('rules', array(
                'label'     => Mage::helper('simplebundle')->__('Template association rules'),
                'title'     => Mage::helper('simplebundle')->__('Template association rules'),
                'content'   => $this->getLayout()->createBlock('simplebundle/adminhtml_template_edit_tab_rule')->toHtml(),
                'active'    => false
            ));
        }

        return parent::_beforeToHtml();
    }

}
