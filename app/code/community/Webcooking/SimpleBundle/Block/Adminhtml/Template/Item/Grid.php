<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.enjalbert.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */

class Webcooking_SimpleBundle_Block_Adminhtml_Template_Item_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('simplebundleTemplateItemGrid');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $model = Mage::registry('simplebundle_template');
        $templateCollection = Mage::getModel('simplebundle/bundle_template_item')->getCollection();
        $productNameAttributeId = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'name')->getId();

        $templateCollection->getSelect()
                ->joinLeft(
                        array('sku_table' => Mage::getConfig()->getTablePrefix() . 'catalog_product_entity'), 'sku_table.entity_id=product_id', array('sku' => 'sku')
                );
        
        $templateCollection->getSelect()
                ->joinLeft(
                        array('name_table' => Mage::getConfig()->getTablePrefix() . 'catalog_product_entity_varchar'), 'name_table.entity_id=product_id and name_table.attribute_id = ' . $productNameAttributeId, array('name' => 'value')
                );
        
        $templateCollection->addFieldToFilter('template_id', $model->getId());
        
        $templateCollection->getSelect()->group('template_item_id');
        $this->setCollection($templateCollection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        

        $this->addColumn('template_item_id', array(
            'header'    => Mage::helper('simplebundle')->__('ID'),
            'align'     => 'left',
            'index'     => 'template_item_id',
            'type'	=> 'number'
        ));
        
        $this->addColumn('product', array(
            'header'    => Mage::helper('simplebundle')->__('Product'),
            'align'     => 'left',
            'index'     => 'product',
            'filter'    => false,
            'frame_callback' => array($this, 'decorateProduct')
        ));
        
        $this->addColumn('selection_qty', array(
            'header'    => Mage::helper('simplebundle')->__('Qty'),
            'align'     => 'left',
            'index'     => 'selection_qty',
            'type'      => 'number'
        ));
        
        $this->addColumn('selection_position', array(
            'header'    => Mage::helper('simplebundle')->__('Position'),
            'align'     => 'left',
            'index'     => 'position',
            'type'      => 'number'
        ));
        
        return parent::_prepareColumns();
    }


    /**
     * Row click url
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return "javascript:loadEditProductPopup('".$row->getId()."')";
    }
    
     public function getGridUrl()
    {
        return $this->getUrl('adminhtml/simplebundle_template_item/grid', array('templateId'=>Mage::registry('simplebundle_template')->getId()));
    }
    
    public function decorateProduct($value, $row, $column, $isExport) {
        $value  = $row->getSku();
        $value .= '<br/>';
        $value .= $row->getName();
        return $value;
    }
    
    
    
}
