<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */

if(Mage::helper('wcooall')->isModuleInstalled('OrganicInternet_SimpleConfigurableProducts')) {
    class Webcooking_SimpleBundle_Block_Catalog_Product_View_Bundle_Popup_Product_Type_Configurable_Compatibility extends OrganicInternet_SimpleConfigurableProducts_Catalog_Block_Product_View_Type_Configurable {}
} else if(Mage::helper('wcooall')->isModuleInstalled('CJM_CustomStockStatus')) {
    class Webcooking_SimpleBundle_Block_Catalog_Product_View_Bundle_Popup_Product_Type_Configurable_Compatibility extends CJM_CustomStockStatus_Block_Catalog_Product_View_Type_Configurable {}
//} else if(Mage::helper('wcooall')->isModuleInstalled('Amasty_Conf')) {
//    class Webcooking_SimpleBundle_Block_Catalog_Product_View_Bundle_Popup_Product_Type_Configurable_Compatibility extends Amasty_Conf_Block_Catalog_Product_View_Type_Configurable {}
} else {
    class Webcooking_SimpleBundle_Block_Catalog_Product_View_Bundle_Popup_Product_Type_Configurable_Compatibility extends Mage_Catalog_Block_Product_View_Type_Configurable {}
}


class Webcooking_SimpleBundle_Block_Catalog_Product_View_Bundle_Popup_Product_Type_Configurable extends Webcooking_SimpleBundle_Block_Catalog_Product_View_Bundle_Popup_Product_Type_Configurable_Compatibility {


    protected function _getAdditionalConfig()
    {
        return array('containerId'=>'product_addtocart_form-'.$this->getProduct()->getId());
    }
    
}