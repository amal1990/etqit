<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Block_Catalog_Product_View_Bundle_Popup_Product extends Mage_Catalog_Block_Product_View {


    protected $_additionnalHtml = '';
    
    
    protected function _prepareLayout() {

    }
    
    public function setProductId($productId) {
        $this->setData('product_id', $productId);
        if(Mage::registry('product')) Mage::unregister('product');
        Mage::register('product', Mage::getModel('catalog/product')->load(intval($this->getProductId())));
        if(Mage::registry('current_product')) Mage::unregister('current_product');
        Mage::register('current_product', Mage::registry('product'));
        $this->getLayout()->getBlock('product.info.options')->setProduct(Mage::registry('product'));
        $this->getLayout()->getBlock('product.info.configurable')->setProduct(Mage::registry('product'));
        $this->getLayout()->getBlock('product.info.simple')->setProduct(Mage::registry('product'));
        
        Mage::registry('product')->setOptionsContainer('container2');
        
        
        $container1 = $this->getLayout()->getBlock('product.info.container1');
        $container1->setDataByKey('alias_in_layout', 'container1');
        $container1->setDataByKeyFromRegistry('options_container', 'product');
        //$container1->unsetChild('product.info.options.wrapper')->unsetChild('product.info.options.wrapper.bottom');
        $container1->append('product.info.options.wrapper')
                   ->append('product.info.options.wrapper.bottom');
        $container2 = $this->getLayout()->getBlock('product.info.container2');
        $container2->setDataByKey('alias_in_layout', 'container2');
        $container2->setDataByKeyFromRegistry('options_container', 'product');
        //$container2->unsetChild('product.info.options.wrapper')->unsetChild('product.info.options.wrapper.bottom');
        $container2->append('product.info.options.wrapper')
                   ->append('product.info.options.wrapper.bottom');
        
        $this->getLayout()->getBlock('product.info.options.wrapper')->unsetChild('product.info.options.configurable');
        $this->getLayout()->getBlock('product.info.options.wrapper')->unsetChild('product.info.options.bundle');
        $this->getLayout()->getBlock('product.info.options.wrapper.bottom')->unsetChild('bundle.tierprices');
        
        if(Mage::registry('product')->getTypeId() == 'configurable') {
            if(false && Mage::helper('wcooall')->isModuleInstalled('CJM_ColorSelectorPlus')) {
                $mediaBlock = $this->getLayout()->createBlock('catalog/product_view_media', 'product.info.media');
                $mediaBlock->setTemplate('colorselectorplus/catalog/product/view/media.phtml');
                $this->_additionnalHtml .= '<div style="display:none">'.$mediaBlock->toHtml().'</div>';
                $configurableBlock = $this->getLayout()->createBlock('colorselectorplus/catalog_product_view_type_configurable', 'product.info.options.configurable');
                $configurableBlock->setTemplate('colorselectorplus/catalog/product/view/type/options/configurable.phtml');
            } else {
                $configurableBlock = $this->getLayout()->createBlock('simplebundle/catalog_product_view_bundle_popup_product_type_configurable', 'product.info.options.configurable');
                $configurableBlock->setTemplate('catalog/product/view/type/options/configurable.phtml');
            }
            /*compatibility with rwd theme*/
            $attrRenderersBlock = $this->getLayout()->createBlock("core/text_list", 'product.info.options.configurable.renderers');
            $afterBlock = $this->getLayout()->createBlock("core/text_list", 'product.info.options.configurable.after');
            $configurableBlock->setChild('attr_renderers', $attrRenderersBlock);
            $configurableBlock->setChild('after', $afterBlock);
            /*compatibility with rwd theme end */
            $configurableBlock->setBefore('-');
            $this->getLayout()->getBlock('product.info.options.wrapper')->append($configurableBlock);
        }
        
        if(Mage::registry('product')->getTypeId() == 'bundle') {
            $bundleBlock = $this->getLayout()->createBlock('simplebundle/catalog_product_view_bundle_popup_product_type_bundle', 'product.info.options.bundle');
            $bundleBlock->setTemplate('bundle/catalog/product/view/type/bundle/options.phtml');
            $bundleBlock->addRenderer('select', 'bundle/catalog_product_view_type_bundle_option_select');
            $bundleBlock->addRenderer('multi', 'bundle/catalog_product_view_type_bundle_option_multi');
            $bundleBlock->addRenderer('radio', 'bundle/catalog_product_view_type_bundle_option_radio');
            $bundleBlock->addRenderer('checkbox', 'bundle/catalog_product_view_type_bundle_option_checkbox');
            $this->getLayout()->getBlock('product.info.options.wrapper')->insert('product.info.options.bundle');
            
            $bundleTPBlock = $this->getLayout()->createBlock('bundle/catalog_product_view', 'bundle.tierprices');
            $bundleTPBlock->setTemplate('bundle/catalog/product/view/tierprices.phtml');
            $this->getLayout()->getBlock('product.info.options.wrapper.bottom')->append($bundleTPBlock);
           
            if(Mage::helper('wcooall')->isModuleInstalled('Robbie_Bundleoption')) {
                if(Mage::getStoreConfigFlag('bundleoption_section_one/group_1/field_checkbox')) {
                    $bundleBlock->addRenderer('checkbox', 'bundleoption/catalog_product_view_type_bundle_option_checkbox');
                }
                if(Mage::getStoreConfigFlag('bundleoption_section_one/group_1/field_radio')) {
                    $bundleBlock->addRenderer('radio', 'bundleoption/catalog_product_view_type_bundle_option_radio');
                }
            }
            
        }
        
        $this->unsetCallChild('container1', 'ifEquals', 0, 'alias_in_layout', 'options_container');
        $this->unsetCallChild('container2', 'ifEquals', 0, 'alias_in_layout', 'options_container');
        
        
        
        return $this;
    }
    
    protected function _loadCache() {
        return false;
    }
    
    protected function _getChildHtml($name, $useCache = true)
    {
        $child = $this->getChild($name);
        if ($child) {
            $child->setCacheLifetime(null);
        }
        return parent::_getChildHtml($name, false);
    }
    
    protected function _toHtml() {
        $html = parent::_toHtml();
        $html = Mage::helper('simplebundle')->distinguishVariables($html, $this->getProduct()->getId());
        return $html . $this->_additionnalHtml;
    }
    
    
    
}