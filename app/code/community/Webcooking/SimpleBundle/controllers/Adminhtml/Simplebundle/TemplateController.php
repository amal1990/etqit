<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Adminhtml_Simplebundle_TemplateController extends Mage_Adminhtml_Controller_Action {

    protected function _construct() {
        $this->setUsedModuleName('Webcooking_SimpleBundle');
    }

    protected function _initAction() {
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
                ->_setActiveMenu('catalog/simplebundle')
                ->_addBreadcrumb(Mage::helper('simplebundle')->__('Templates'), Mage::helper('simplebundle')->__('Templates'))
        ;
        return $this;
    }

    public function indexAction() {
        $this->loadLayout()
                ->_addContent($this->getLayout()->createBlock('simplebundle/adminhtml_template'))
                ->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function editAction() {
        $id = $this->getRequest()->getParam('template_id');
        $model = Mage::getModel('simplebundle/bundle_template');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('simplebundle')->__('This bundle does not exist'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('simplebundle_template', $model);

        $this->_initAction()
                ->_addBreadcrumb($id ? Mage::helper('simplebundle')->__('Edit template') : Mage::helper('simplebundle')->__('New template'), $id ? Mage::helper('simplebundle')->__('Edit template') : Mage::helper('simplebundle')->__('New template'))
                ->_addContent($this->getLayout()->createBlock('simplebundle/adminhtml_template_edit')->setData('action', $this->getUrl('*/*/save')))
                ->_addLeft($this->getLayout()->createBlock('simplebundle/adminhtml_template_edit_tabs'))
        ;

        $this->renderLayout();
    }

    public function saveAction() {
        if ($data = $this->getRequest()->getPost()) {
     
            $data['name'] = $data['template_name'];
            $model = Mage::getModel('simplebundle/bundle_template');
            if(!isset($data['stores']) || !$data['stores'] || $data['all_stores']) {
                $data['stores'] = '0';
            } else {
                if (Mage::app()->isSingleStoreMode())
                    $data['stores'] = Mage::app()->getDefaultStoreView()->getId();
                else
                    $data['stores'] = implode(',', $data['stores']);
            }
            
            $data['conditions'] = $data['rule']['conditions'];
            unset($data['rule']);
            $rule = $model->getRule();
            $rule->loadPost($data);
            if ($rule->getConditions()) {
                $data['conditions_serialized'] = serialize($rule->getConditions()->asArray());
                unset($data['conditions']);
            }
            
            foreach($data as $key=>$val) {
                $model->setData($key, $val);
            }
            if (!$model->getData('template_id'))
                $model->setData('created_at', date('Y-m-d H:i:s'));
            $model->setData('updated_at', date('Y-m-d H:i:s'));

            try {
                $model->save();

                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('simplebundle')->__('Template has been saved'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('template_id' => $model->getId()));
                    return;
                }
                // go to grid
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('template_id' => $this->getRequest()->getParam('template_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }
    
    public function deleteAction() {
        $id = $this->getRequest()->getParam('template_id');
        $model = Mage::getModel('simplebundle/bundle_template');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('simplebundle')->__('This template does not exist'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $model->delete();
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('simplebundle')->__('This template has been deleted'));
        $this->_redirect('*/*/');
        return;
    }

}
