<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_Adminhtml_Simplebundle_ListController extends Mage_Adminhtml_Controller_Action {

    protected function _construct() {
        $this->setUsedModuleName('Webcooking_SimpleBundle');
    }

    public function indexAction() {
        $this->loadLayout()
                ->_addContent($this->getLayout()->createBlock('simplebundle/adminhtml_list'))
                ->renderLayout();
    }

    public function exportCsvAction() {
        //ini_set('memory_limit', '1024M');
        //ini_set('display_errors', 1);
        $fileName = 'simple-bundles-' . date('Y-m-d--H-i-s') . '.csv';
        $grid = $this->getLayout()->createBlock('simplebundle/adminhtml_list_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    protected function _checkEntetes($entetes) {
        if (!in_array('sku', $entetes)) {
            return false;
        }
        if (!in_array('discount_amount', $entetes)) {
            return false;
        }
        return true;
    }

    protected function _parseImportDataLine($dataLine) {
        $session = Mage::getSingleton('adminhtml/session');
        if (!is_array($dataLine)) {
            return false;
        }
        if (!isset($dataLine['stores']) || !$dataLine['stores']) {
            $dataLine['stores'] = 0;
        }
        if (!isset($dataLine['active'])) {
            $dataLine['active'] = 1;
        }
        if (!isset($dataLine['base_qty'])) {
            $dataLine['base_qty'] = 1;
        }
        if (!isset($dataLine['discount_type'])) {
            $dataLine['discount_type'] = 'percent';
        }
        if (!isset($dataLine['position'])) {
            $dataLine['position'] = 0;
        }
        if (!isset($dataLine['exclude_base_product_from_discount'])) {
            $dataLine['exclude_base_product_from_discount'] = Mage::helper('simplebundle')->excludeBaseProductFromDiscount()?'1':'0';
        }
        if (!isset($dataLine['special_price_behavior'])) {
            $dataLine['special_price_behavior'] = Mage::helper('simplebundle')->getSpecialPriceBehavior();
        }
        $productId = Mage::getModel('catalog/product')->getIdBySku($dataLine['sku']);
        if (!$productId) {
            $session->addError(Mage::helper('simplebundle')->__('SKU %s not found', $dataLine['sku']));
            return false;
        }
        $dataLine['product_id'] = $productId;

        for ($i = 1; $i <= 100; $i++) {
            if (isset($dataLine['simple_bundle_item_sku_' . $i]) && $dataLine['simple_bundle_item_sku_' . $i]) {
                $productId = Mage::getModel('catalog/product')->getIdBySku($dataLine['simple_bundle_item_sku_' . $i]);
                if (!$productId) {
                    $session->addError(Mage::helper('simplebundle')->__('SKU %s not found', $dataLine['sku']));
                    return false;
                }
                if ($productId == $dataLine['product_id']) {
                    $session->addError(Mage::helper('simplebundle')->__('Subproduct cannot be the same as master product'));
                    return false;
                }
                $dataLine['simple_bundle_item_product_id_' . $i] = $productId;
                if (!isset($dataLine['simple_bundle_item_qty_' . $i])) {
                    $dataLine['simple_bundle_item_qty_' . $i] = 1;
                }
                if (!isset($dataLine['simple_bundle_item_position_' . $i])) {
                    $dataLine['simple_bundle_item_position_' . $i] = 0;
                }
            }
        }


        return $dataLine;
    }

    public function importCsvAction() {
        //ini_set('display_errors', 1);
        //ini_set('memory_limit', '1024M');
        $session = Mage::getSingleton('adminhtml/session');
        if ($data = $this->getRequest()->getPost()) {
            if (!$_FILES['simplebundle_import']['tmp_name']) {
                $session->addError(
                        Mage::helper('simplebundle')->__('No file found.'));
                $this->_redirect('*/*/');
                return;
            }

            $handle = fopen($_FILES['simplebundle_import']['tmp_name'], 'r');

            $entetes = $fileData = fgetcsv($handle, null, ",");
            if (!$this->_checkEntetes($entetes)) {
                $session->addError(
                        Mage::helper('simplebundle')->__('File columns are not correct.'));
                $this->_redirect('*/*/');
                return;
            }
            $cpt = 0;
            $cptSuccess = 0;
            while (($fileData = fgetcsv($handle, null, ",")) !== FALSE) {
                $cpt++;
                $fileData = array_combine($entetes, $fileData);

                $fileData = $this->_parseImportDataLine($fileData);
                if ($fileData) {

                    $bundle = Mage::getModel('simplebundle/bundle');
                    if (isset($fileData['simple_bundle_id']) && $fileData['simple_bundle_id']) { //update
                        $bundle->load($fileData['simple_bundle_id']);
                    } else {
                        $bundle->setCreatedAt(date('Y-m-d H:i:s'));
                    }
                    $bundle->setUpdatedAt(date('Y-m-d H:i:s'));
                    unset($fileData['simple_bundle_id']);
                    unset($fileData['id']); //just in case..

                    foreach ($fileData as $key => $val) {
                        $bundle->setData($key, $val);
                    }
                    $bundle->save();
                    for ($i = 1; $i <= 100; $i++) {
                        if (isset($fileData['simple_bundle_item_sku_' . $i]) && $fileData['simple_bundle_item_sku_' . $i]) {
                            $bundleItem = Mage::getModel('simplebundle/bundle_item');
                            if (isset($fileData['simple_bundle_item_id_' . $i]) && $fileData['simple_bundle_item_id_' . $i]) {
                                $bundleItem->load($fileData['simple_bundle_item_id_' . $i]);
                                if ($bundleItem->getSimpleBundleId() != $bundle->getSimpleBundleId()) {
                                    $session->addError(Mage::helper('simplebundle')->__('Simple Bundle Item #%s does not match Simple Bundle #s', $fileData['simple_bundle_item_id_' . $i], $bundle->getSimpleBundleId()));
                                    continue;
                                }
                            } else {
                                $bundleItem->setSimpleBundleId($bundle->getId());
                            }
                            
                            $bundleItem->setData('product_id', $fileData['simple_bundle_item_product_id_' . $i]);
                            $bundleItem->setData('selection_qty', $fileData['simple_bundle_item_qty_' . $i]);
                            $bundleItem->setData('position', $fileData['simple_bundle_item_position_' . $i]);
                            $bundleItem->save();
                        }
                    }
                    $cptSuccess++;
                }
            }
            $session->addSuccess(Mage::helper('simplebundle')->__('%s simple bundles on %s have been imported or updated', $cptSuccess, $cpt));
            $this->_redirect('*/*/');
            return;
        }

        $this->loadLayout()
                ->renderLayout();
    }

}
