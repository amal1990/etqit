<?php

/**
 * Vincent Enjalbert
 *
 * Version Française :
 * *****************************************************************************
 *
 * Notification de la Licence
 *
 * Ce fichier source est sujet au CLUF
 * qui est fourni avec ce module dans le fichier LICENSE-FR.txt.
 * Il est également disponible sur le web à l'adresse suivante:
 * http://www.web-cooking.net/licences/magento/LICENSE-FR.txt
 *
 * =============================================================================
 *        NOTIFICATION SUR L'UTILISATION DE L'EDITION MAGENTO
 * =============================================================================
 * Ce module est conçu pour l'édition COMMUNITY de Magento
 * WebCooking ne garantit pas le fonctionnement correct de cette extension
 * sur une autre édition de Magento excepté l'édition COMMUNITY de Magento.
 * WebCooking ne fournit pas de support d'extension en cas
 * d'utilisation incorrecte de l'édition.
 * =============================================================================
 *
 * English Version :
 * *****************************************************************************
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE-EN.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 *
 * =============================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =============================================================================
 * This package designed for Magento COMMUNITY edition
 * WebCooking does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * WebCooking does not provide extension support in case of
 * incorrect edition usage.
 * =============================================================================
 *
 * @category   Webcooking
 * @package    Webcooking_SimpleBundle
 * @copyright  Copyright (c) 2011-2015 Vincent René Lucien Enjalbert
 * @license    http://www.web-cooking.net/licences/magento/LICENSE-EN.txt
 */
class Webcooking_SimpleBundle_CartController extends Mage_Core_Controller_Front_Action {

    protected function _getCart() {
        return Mage::getSingleton('checkout/cart');
    }
    
   
    protected function _getSession() {
        return Mage::getSingleton('checkout/session');
    }

    protected function _goBack() {
        $returnUrl = $this->getRequest()->getParam('return_url');
        if ($returnUrl) {

            if (!$this->_isUrlInternal($returnUrl)) {
                throw new Mage_Exception('External urls redirect to "' . $returnUrl . '" denied!');
            }

            $this->_getSession()->getMessages(true);
            $this->getResponse()->setRedirect($returnUrl);
        } elseif (!Mage::getStoreConfig('checkout/cart/redirect_to_cart') && !$this->getRequest()->getParam('in_cart') && $backUrl = $this->_getRefererUrl()
        ) {
            $this->getResponse()->setRedirect($backUrl);
        } else {
            if (($this->getRequest()->getActionName() == 'add') && !$this->getRequest()->getParam('in_cart')) {
                $this->_getSession()->setContinueShoppingUrl($this->_getRefererUrl());
            }
            $this->_redirect('checkout/cart');
        }
        return $this;
    }

    // if bundle with only default options, add these automatically
    

    public function addAction() {
        $cart = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            
            $bundleId = $params['selection'];
            if (!$bundleId) {
               $this->_goBack();
               return;
           }
           $bundleId = unserialize(base64_decode($bundleId));

            if(!Mage::helper('simplebundle')->addBundleToCart($cart, $bundleId, $params)) {
                $this->_goBack();
               return;
            }
            
            
            
            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    $message = $this->__('Bundle was added to your shopping cart.');
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }
    }

    public function priceupdateAction() {
        $this->getLayout()->getUpdate()->addHandle('catalog_product_view');
        $this->loadLayout();
        Mage::register('product', Mage::getModel('catalog/product')->load($this->getRequest()->getParam('productId')));
        Mage::register('sb_options_prices', explode(';', $this->getRequest()->getParam('optionPrice')));
        echo $this->getLayout()->getBlock('simple.bundles')->toHtml();
        exit();
    }

    public function addtocartpopupAction() {
        $bundle = Mage::getModel('simplebundle/bundle')->load($this->getRequest()->getParam('bundle_id'));
        $this->loadLayout();
        $this->getLayout()->getBlock('simple.bundles.popup')->setBundle($bundle);
        if(Mage::helper('wcooall')->isModuleInstalled('OrganicInternet_SimpleConfigurableProducts')) {
            $this->getLayout()->getBlock('product.info.options.wrapper')->setTemplate('catalog/product/view/options/scpwrapper.phtml');
            $this->getLayout()->getBlock('product.info.options')->setTemplate('catalog/product/view/scpoptions.phtml');
        }
        $this->renderLayout();
    }

    public function addfrompopupAction() {
        $cart = $this->_getCart();
        $params = $this->getRequest()->getParams();
        try {
            $bundleId = $params['bundle_id'];
            foreach ($params['sbdata'] as $productToAdd) {
                parse_str(urldecode($productToAdd), $productRequest);
                $product = Mage::getModel('catalog/product')
                        ->setStoreId(Mage::app()->getStore()->getId())
                        ->load($productRequest['product']);
                $cart->addProduct($product, $productRequest);
            }
            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            $simpleBundleIds = $this->_getSession()->getSimpleBundleIds();
            if (!$simpleBundleIds) {
                $simpleBundleIds = array();
            }
            $simpleBundleIds[] = $bundleId;
            $this->_getSession()->setSimpleBundleIds($simpleBundleIds);

            echo '';
            die();
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                echo Mage::helper('core')->escapeHtml($e->getMessage());
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    echo Mage::helper('core')->escapeHtml($message) . "<br/>";
                }
            }
        } catch (Exception $e) {
            echo $this->__('Cannot add the item to shopping cart.');
            Mage::logException($e);
        }
    }

}
