<?php
require_once "Mage/Catalog/controllers/Product/CompareController.php";
class Uipl_Coreextended_Product_CompareController extends Mage_Catalog_Product_CompareController
{
    /**
     * Add item to compare list
     */
    public function addAction()
    {
        if (!$this->_validateFormKey()) {
            $this->_redirectReferer();
            return;
        }
        
        //amal change compare products limit
        
        $maxCount = 5;
        $productId = (int) $this->getRequest()->getParam('product');
        if ($productId
            && (Mage::getSingleton('log/visitor')->getId() || Mage::getSingleton('customer/session')->isLoggedIn())
        ) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId(Mage::app()->getStore()->getId())
                ->load($productId);
        $count1 = count($product);
        $count = Mage::helper('catalog/product_compare')->getItemCount();
          if ($count < $maxCount)
            {        
                    if ($product->getId()/* && !$product->isSuper()*/) {
                        Mage::getSingleton('catalog/product_compare_list')->addProduct($product);
                        Mage::getSingleton('catalog/session')->addSuccess(
                            $this->__('The product %s has been added to comparison list.', Mage::helper('core')->escapeHtml($product->getName()))
                        );
                        Mage::dispatchEvent('catalog_product_compare_add_product', array('product'=>$product));
                    }
        
                    Mage::helper('catalog/product_compare')->calculate();
            }
                 
        else 
            {  
             Mage::getSingleton('catalog/session')->addError($this->__('The maximum number of products to compare is %s', $maxCount));
            } 
             }

        $this->_redirectReferer();
    }
}
