<?php
	
class Uipl_Specialoffer_Block_Adminhtml_Specialoffer_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "specialoffer_id";
				$this->_blockGroup = "specialoffer";
				$this->_controller = "adminhtml_specialoffer";
				$this->_updateButton("save", "label", Mage::helper("specialoffer")->__("Save Item"));
				$this->_updateButton("delete", "label", Mage::helper("specialoffer")->__("Delete Item"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("specialoffer")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("specialoffer_data") && Mage::registry("specialoffer_data")->getId() ){

				    return Mage::helper("specialoffer")->__("Edit Item '%s'", $this->htmlEscape(Mage::registry("specialoffer_data")->getId()));

				} 
				else{

				     return Mage::helper("specialoffer")->__("Add Item");

				}
		}
}