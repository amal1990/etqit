<?php
class Uipl_Specialoffer_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Specialoffer"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));

      $breadcrumbs->addCrumb("specialoffer", array(
                "label" => $this->__("Specialoffer"),
                "title" => $this->__("Specialoffer")
		   ));

      $this->renderLayout(); 
	  
    }
}