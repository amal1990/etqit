<?php
class Uipl_Ak_IndexController extends Mage_Core_Controller_Front_Action
{
    public function IndexAction()
    {
	$this->loadLayout();   
        $this->renderLayout(); 
    }
    
    public function sendalertAction(){
		
		$configValue = Mage::getStoreConfig('general/stockalert/active');
		if($configValue!=1){
			Mage::log("Stock alert is disabled from admin general system configuration");
			return false;
		}
		$emailTemplate  = Mage::getModel('core/email_template')
						->loadDefault('outofstock_email_template');
            $collection = Mage::getModel('catalog/product')
                        ->getCollection()->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                        ->addAttributeToSelect('*')->addAttributeToFilter('status', array('eq'=>'1'));
			
			 $expro='<table width="100%" cellpadding="10" cellspacing="10" style="    border: 1px #ccc solid;">
                                    <tr><td style="font-family: Verdana, Arial;
    font-weight: normal;font-size:14px;padding:5px 5px 5px 10px;width:20%;border-right:1px #ccc solid;border-bottom:1px #ccc solid; background:#e1dfdf;">Product Id</td>
	<td style="font-family: Verdana, Arial;
    font-weight: normal;font-size:14px;padding:5px 5px 5px 10px;width:25%;border-right:1px #ccc solid;border-bottom:1px #ccc solid; background:#e1dfdf;">Product Name</td><td style="font-family: Verdana, Arial;
    font-weight: normal;font-size:14px;padding:5px 5px 5px 10px;width:15%;border-right:1px #ccc solid;border-bottom:1px #ccc solid; background:#e1dfdf;">SKU</td><td style="font-family: Verdana, Arial;
    font-weight: normal;font-size:14px;padding:5px 5px 5px 10px;width:20%; background:#e1dfdf;border-bottom:1px #ccc solid;">Product Url</td></tr>
                                ';
                            $pdata='';
			foreach($collection as $_product){
                            //echo $_product->getId().'<br>';
				 $_product = Mage::getModel('catalog/product')->load($_product->getId());
                                 
				 $num= Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getQty();
				 $qtyStock=Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getMinQty();
                                 //if($_product->getId()==2)
                                 //{
                                 //   echo number_format($num,0);
                                 //   echo $qtyStock;
                                 //}
				//$qtyStock=$qtyStock-$orderqty;
					     if(number_format($num,0)<=$qtyStock){
						
						  $expro .='<tr><td style="font-family: Verdana, Arial;
    font-weight: normal;font-size:14px;padding:15px 5px 15px 10px;width:20%;border-right:1px #ccc solid;">'.$_product->getId().'</td><td style="font-family: Verdana, Arial;
    font-weight: normal;font-size:14px;padding:15px 5px 15px 10px;width:25%;border-right:1px #ccc solid;">'.$_product->getName().'</td><td style="font-family: Verdana, Arial;
    font-weight: normal;font-size:14px;padding:15px 5px 15px 10px;width:15%;border-right:1px #ccc solid;">'.$_product->getSku().'</td>
	<td style="font-family: Verdana, Arial;
    font-weight: normal;font-size:14px;padding:15px 5px 15px 10px;width:20%;color:#615a5a;"><a style="color:#615a5a;" href="'.$_product->getProductUrl().'">'.$_product->getProductUrl().'</a></td></tr>';
                                                $pdata='ok';
					     }
				
			}
			
			 $expro .='</table>';
			 
			 $email_to=Mage::getStoreConfig('trans_email/ident_sales/email');
			 //$email_to .=",".Mage::getStoreConfig('general/stockalert/otheremails');
			  $email_template_variables = array(
                                 'alertGrid' => $expro,
                                
                                 
                                 );
                                 
                                 if($pdata!='')
                                 {
                                    $sender_name = Mage::getStoreConfig('trans_email/ident_general/name'); ;
                                 
                                    $sender_email = Mage::getStoreConfig('trans_email/ident_general/email');
                                    
                                   $emailTemplate->setSenderName($sender_name);
                                   $emailTemplate->setSenderEmail( $sender_email);
                                    
                                     $processedTemplate = $emailTemplate->getProcessedTemplate($email_template_variables);
                                  
                                   $emailTemplate->send($email_to,'Product Low Stock Notification', $email_template_variables);
                                 }
            
            echo 'ok';exit;
               
	} 
    
    
    public function prodlistAction()
    {
	if ($_REQUEST['start'])
	{
	    $array='';
	    $wish_url='';
	    $com_url='';
	    $price_list=0;
	    $lim=4;
	    $lim0 = $_POST['start'];
	    $lim0 = ($lim0 * $lim);
		
	    $collection_limit = Mage::getModel('catalog/product')->getCollection();
	    $collection_limit->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
	    $collection_limit->joinField('qty',
			     'cataloginventory/stock_item',
			     'qty',
			     'product_id=entity_id',
			     '{{table}}.stock_id=1',
			     'left')
		 ->addAttributeToFilter('qty', array("gt" => 0));
	    $collection_limit->addFieldToFilter('featured_product', '1')->getSelect()->limit($lim,$lim0);
	    
	    $limt_collection=$collection_limit->count();
	    if($limt_collection > 0)
	    {
		//$array.='<div class="row">';
		
		$attr_arr1=array();
		foreach ($collection_limit as $product)
		{
		    $attr_arr1=array();
		    $_product = Mage::getModel('catalog/product')->load($product->getId());
		    
		    if($_product->getSpecialPrice()>0)
		    {
			$_formattedActualPrice = '<span>'.Mage::helper('core')->currency($_product->getPrice(),true,false).'</span>'; 
			$_formattedSpecialPrice = Mage::helper('core')->currency($_product->getSpecialPrice(),true,false);
			
			$price_list=$_formattedActualPrice.$_formattedSpecialPrice;
		    }else
		    {
			$_formattedActualPrice = Mage::helper('core')->currency($_product->getPrice(),true,false);
			$price_list=$_formattedActualPrice;
		    }
		    
		    if (Mage::helper('wishlist')->isAllow()) : 
		    $wish_url='<a class="icon" href="'.Mage::helper('wishlist')->getAddUrl($_product).'"><span><img src="'. Mage::getDesign()->getSkinUrl().'images/like.png'.'" alt=""></span></a>';
		    endif;
		    if ($_compareUrl = Mage::helper('catalog/product_compare')->getAddUrl($_product)):
		    $com_url='<a class="icon" href="'.$_compareUrl.'"><span><img src="'.Mage::getDesign()->getSkinUrl().'images/compare.png'.'" alt=""></span></a>';
		    endif; 
		    
		    $array.='<div class="col-sm-6 col-md-3">
			<div class="list_pan img_cont">
			    <div class="pro_con">
			    <div class="product-box-con">';
				if(!$_product->isSaleable()):
				$array.= '<div class="bought"><span></span>'.$this->__('OUT OF STOCK').'</div>';
				endif;
				$array.='<div class="product-box">
				    <div class="product-image">
				    <a href="'.$_product->getProductUrl().'">
				    <img src="'.Mage::helper('catalog/image')->init($_product, 'small_image')->resize(225,145).'" alt="'.Mage::helper('core')->stripTags($_product->getName(), null, true).'"/>
				    </a>
				    </div>
				</div>
			    </div>
			    <div class="listing_content text-center">
				<h4><a href="'.$_product->getProductUrl() .'">'. $_product->getName().'</a></h4>';
				?>
			<p>	
				<?php if($_product->getBrand()!=''){ ?>
		
				<?php
						$attr = $_product->getResource()->getAttribute("brand");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getBrand());
				?>

		<?php } ?>
		
		<?php if($_product->getMake()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("make");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getMake());
				?>

		<?php } ?>
		
		<?php if($_product->getModel()!=''){ ?>

				<?php
				$attr_arr1[]= $_product->getModel();
				?>

		<?php } ?>
		
		<?php if($_product->getProcessor()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("processor");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getProcessor());
				?>

		<?php } ?>
		
		<?php if($_product->getInstalledMemoryRAM()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("installed_memory_ram");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getInstalledMemoryRam());
				?>

		<?php } ?>
		
		<?php if($_product->getDisplaySizeDiagonal()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("display_size_diagonal");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getDisplaySizeDiagonal());
				?>

		<?php } ?>
		
		<?php if($_product->getOperatingsystem()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("operatingsystem");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getOperatingsystem());
				?>

		<?php } ?>
		
		<?php if($_product->getHarddrivesize()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("harddrivesize");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getHarddrivesize());
				?>	

		<?php } ?>
		
		<?php if($_product->getLaptopBagSize()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("laptop_bag_size");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getLaptopBagSize());
				?>	

		<?php } ?>
		
		<?php if($_product->getColour()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("colour");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getColour());
				?>	

		<?php } ?>
		
		<?php if($_product->getDisplaySize()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("display_size");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getDisplaySize());
				?>	

		<?php } ?>
		
		<?php if($_product->getCamera()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("camera");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getCamera());
				?>	

		<?php } ?>
		
		<?php if($_product->getInternalStorage()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("internal_storage");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getInternalStorage());
				?>	

		<?php } ?>
		
		<?php if($_product->getColourOrMono()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("colour_or_mono");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getColourOrMono());
				?>	

		<?php } ?>
		
		<?php if($_product->getMaxPrintSpeed()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("max_print_speed");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getMaxPrintSpeed());
				?>	

		<?php } ?>
		
		<?php if($_product->getWiredNetwork()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("wired_network");
                        $is_wired=$attr->getSource()->getOptionText($_product->getWiredNetwork());
                        if($is_wired=='Yes'){
                            $attr_arr1[] = 'Wired Network';
                        }
				?>	

		<?php } ?>
		
		<?php if($_product->getWirelssNetwork()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("wirelss_network");
                        $is_wireless=$attr->getSource()->getOptionText($_product->getWirelssNetwork());
                        if($is_wireless=='Yes'){
                            $attr_arr1[] = 'Wireless Network';
                        }
				?>	

		<?php } ?>
		
		<?php if($_product->getDisplaySizeDiagonal()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("display_size_diagonal");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getDisplaySizeDiagonal());
				?>	

		<?php } ?>
		
		<?php if($_product->getResolution()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("resolution");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getResolution());
				?>	

		<?php } ?>
		
		<?php if($_product->getAspectRatio()!=''){ ?>
		
				<?php
						$attr = $_product->getResource()->getAttribute("aspect_ratio");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getAspectRatio());
				?>	
		<?php } ?>
		<?php
		if(!empty($attr_arr1)):
			$array.= implode(' | ',$attr_arr1);
			endif;
		?>
			</p>	
				<?php
				$array.='<p class="price_listing pull-left">
				    '.$price_list.'
				</p>
			    </div>
			    
			</div>
		    </div>
		    </div>';
		}
		//$array.='</div>';
	    }
	    echo $array;
	}
	exit;
    }
    
    public function offerprodlistAction()
    {
	if ($_REQUEST['start'])
	{
	    $array='';
	    $wish_url='';
	    $com_url='';
	    $price_list=0;
	    $lim=4;
	    $lim0 = $_POST['start'];
	    $lim0 = ($lim0 * $lim);
		
	    $_Special_productCollection = Mage::getModel('catalog/product')->getCollection();
	    $_Special_productCollection->addAttributeToSelect(array(
			       '*'
	       ))
	       ->addFieldToFilter('visibility', array(
			   Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
			   Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG
	       )) //showing just products visible in catalog or both search and catalog
	       ->addFinalPrice()
	       ->getSelect()
	       ->where('price_index.final_price < price_index.price')->limit($lim,$lim0);
	    
	    $limt_collection=$_Special_productCollection->count();
	    if($limt_collection > 0)
	    {
			$attr_arr1=array();
		//$array.='<div class="row">';
		foreach ($_Special_productCollection as $product)
		{
		    $attr_arr1=array();
		    $_product = Mage::getModel('catalog/product')->load($product->getId());
		    
		    if($_product->getSpecialPrice()>0)
		    {
			$_formattedActualPrice = '<span>'.Mage::helper('core')->currency($_product->getPrice(),true,false).'</span>'; 
			$_formattedSpecialPrice = Mage::helper('core')->currency($_product->getSpecialPrice(),true,false);
			
			$price_list=$_formattedActualPrice.$_formattedSpecialPrice;
		    }else
		    {
			$_formattedActualPrice = Mage::helper('core')->currency($_product->getPrice(),true,false);
			$price_list=$_formattedActualPrice;
		    }
		    
		    $save=0;
            $save=$_product->getPrice()-$_product->getSpecialPrice();
		    
		    if (Mage::helper('wishlist')->isAllow()) : 
		    $wish_url='<a class="icon" href="'.Mage::helper('wishlist')->getAddUrl($_product).'"><span><img src="'. Mage::getDesign()->getSkinUrl().'images/like.png'.'" alt=""></span></a>';
		    endif;
		    if ($_compareUrl = Mage::helper('catalog/product_compare')->getAddUrl($_product)):
		    $com_url='<a class="icon" href="'.$_compareUrl.'"><span><img src="'.Mage::getDesign()->getSkinUrl().'images/compare.png'.'" alt=""></span></a>';
		    endif; 
		    
		    $array.='<div class="col-sm-6 col-md-3">
			<div class="list_pan img_cont">
			    <div class="pro_con">
			    <div class="product-box-con">
				<div class="product-box">
				    <div class="product-image">
				    <a href="'.$_product->getProductUrl().'">
				    <img src="'.Mage::helper('catalog/image')->init($_product, 'small_image')->resize(225,145).'" alt="'.Mage::helper('core')->stripTags($_product->getName(), null, true).'"/>
				    </a>
				    </div>
				</div>';
				if(!$_product->isSaleable()):
				$array.= '<div class="bought"><span></span>'.$this->__('OUT OF STOCK').'</div>';
				endif;
				$array.='<div class="save">'.Mage::helper('core')->__('SAVE').' <span>'.Mage::helper('core')->currency($save,true,false).'</span></div>
			    </div>
			    <div class="listing_content text-center">
				<h4><a href="'.$_product->getProductUrl() .'">'. $_product->getName().'</a></h4>';
				
				?>
			<p>	
				<?php if($_product->getBrand()!=''){ ?>
		
				<?php
						$attr = $_product->getResource()->getAttribute("brand");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getBrand());
				?>

		<?php } ?>
		
		<?php if($_product->getMake()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("make");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getMake());
				?>

		<?php } ?>
		
		<?php if($_product->getModel()!=''){ ?>

				<?php
				$attr_arr1[]= $_product->getModel();
				?>

		<?php } ?>
		
		<?php if($_product->getProcessor()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("processor");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getProcessor());
				?>

		<?php } ?>
		
		<?php if($_product->getInstalledMemoryRAM()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("installed_memory_ram");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getInstalledMemoryRam());
				?>

		<?php } ?>
		
		<?php if($_product->getDisplaySizeDiagonal()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("display_size_diagonal");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getDisplaySizeDiagonal());
				?>

		<?php } ?>
		
		<?php if($_product->getOperatingsystem()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("operatingsystem");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getOperatingsystem());
				?>

		<?php } ?>
		
		<?php if($_product->getHarddrivesize()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("harddrivesize");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getHarddrivesize());
				?>	

		<?php } ?>
		
		<?php if($_product->getLaptopBagSize()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("laptop_bag_size");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getLaptopBagSize());
				?>	

		<?php } ?>
		
		<?php if($_product->getColour()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("colour");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getColour());
				?>	

		<?php } ?>
		
		<?php if($_product->getDisplaySize()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("display_size");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getDisplaySize());
				?>	

		<?php } ?>
		
		<?php if($_product->getCamera()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("camera");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getCamera());
				?>	

		<?php } ?>
		
		<?php if($_product->getInternalStorage()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("internal_storage");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getInternalStorage());
				?>	

		<?php } ?>
		
		<?php if($_product->getColourOrMono()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("colour_or_mono");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getColourOrMono());
				?>	

		<?php } ?>
		
		<?php if($_product->getMaxPrintSpeed()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("max_print_speed");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getMaxPrintSpeed());
				?>	

		<?php } ?>
		
		<?php if($_product->getWiredNetwork()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("wired_network");
                        $is_wired=$attr->getSource()->getOptionText($_product->getWiredNetwork());
                        if($is_wired=='Yes'){
                            $attr_arr1[] = 'Wired Network';
                        }
				?>	

		<?php } ?>
		
		<?php if($_product->getWirelssNetwork()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("wirelss_network");
                        $is_wireless=$attr->getSource()->getOptionText($_product->getWirelssNetwork());
                        if($is_wireless=='Yes'){
                            $attr_arr1[] = 'Wireless Network';
                        }
				?>	

		<?php } ?>
		
		<?php if($_product->getDisplaySizeDiagonal()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("display_size_diagonal");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getDisplaySizeDiagonal());
				?>	

		<?php } ?>
		
		<?php if($_product->getResolution()!=''){ ?>

				<?php
						$attr = $_product->getResource()->getAttribute("resolution");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getResolution());
				?>	

		<?php } ?>
		
		<?php if($_product->getAspectRatio()!=''){ ?>
		
				<?php
						$attr = $_product->getResource()->getAttribute("aspect_ratio");
						$attr_arr1[]= $attr->getSource()->getOptionText($_product->getAspectRatio());
				?>	
		<?php } ?>
		<?php
		if(!empty($attr_arr1)):
			$array.= implode(' | ',$attr_arr1);
			endif;
		?>
			</p>
			
				<?php
				$array.='<p class="price_listing pull-left">
				    '.$price_list.'
				</p>
			    </div>
			    
			</div>
		    </div>
		    </div>';
		}
		//$array.='</div>';
	    }
	    echo $array;
	}
	exit;
    }
}

//<div class="pro-overlay">
//				<div class="iconset pro-overlay-iconset">
//				  <a class="icon" href="'. Mage::helper('checkout/cart')->getAddUrl($_product).'"><span><img src="'.Mage::getDesign()->getSkinUrl().'images/cart.png'.'" alt=""></span></a>
//				  '.$wish_url.$com_url.'
//				</div>
//			    </div>


//<div class="pro-overlay">
//				<div class="iconset pro-overlay-iconset">
//				  <a class="icon" href="'. Mage::helper('checkout/cart')->getAddUrl($_product).'"><span><img src="'.Mage::getDesign()->getSkinUrl().'images/cart.png'.'" alt=""></span></a>
//				  '.$wish_url.$com_url.'
//				</div>
//			    </div>
?>