<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('salesrule'),
        'special_promotional_code',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_SMALLINT,
            'length' => 6,
            'nullable' => false,
            'default' => 0,
            'comment' => 'Is Special Promotional Code'
        )
    );

$installer->endSetup();